from collections import defaultdict
from os.path import join
import sklearn.utils as skl
import matplotlib.pyplot as plt
import numpy as np

DEFAULT_ROOT = '/oasis/projects/nsf/csd538/mhnguyen/project-share/ucsd-medical/data/preprocessed'

class DataLoader():

    def __init__(self, root_dir=DEFAULT_ROOT, dataset_dirs=['sunnybrook_180'], 
                 split=[0.75, 0.15, 0.10], stratified=True, shuffle=True, verbose=False):
        """ Class for loading datadasets; call the load method to get a data matrix 
        and a label matrix.        
        """
        #if split is not None:
        #    assert sum(split) == 1.0, 'split {} adds up to {} (must be 1)'.format(split, sum(split))

        
        self.root_path = root_dir
        self.dataset_dirs = dataset_dirs
        self.data_path = join(root_dir, dataset_dirs[0])
        self.split = split
        self.stratified = stratified
        self.shuffle = shuffle
        self.verbose = verbose
    
        # stores dataset currently being processed
        self.curr_dataset = {'name': '', 
                             'data': [],
                             'labels': [],
                             'metadata': []}
 

    def load(self, verbose=False):
        """ Iterates over datasets specified in self.dataset_dirs, loads (potentially
        shuffling and splitting) and merges them together.
        """
        if verbose: 
            self.verbose = verbose
            
        if self.split is not None:
            dataset = {'data': {'train': [], 'val': [], 'test': []},
                       'labels': {'train': [], 'val': [], 'test': []},
                       'metadata': {'train': [], 'val': [], 'test': []}}
        else:
            dataset = {'data': [],
                       'labels': [],
                       'metadata': []}
        
        for dataset_dir in self.dataset_dirs:
            self.curr_dataset['name'] = dataset_dir
            self.data_path = join(self.root_path, dataset_dir)
            self._load_dataset(dataset_dir) 
            if self.split:
                for s in ['train', 'val', 'test']:
                    dataset['data'][s] += self.curr_dataset['data'][s]
                    dataset['labels'][s] += self.curr_dataset['labels'][s]
                    dataset['metadata'][s] += self.curr_dataset['metadata'][s]
            else:
                dataset['data'] += self.curr_dataset['data']
                dataset['labels'] += self.curr_dataset['labels']
                dataset['metadata'] += self.curr_dataset['metadata']
                
            self.clear_curr_dataset(with_split=False)
        
        self.curr_dataset = dataset
        self._print("\n\t----- Merged {} datasets -----".format(len(self.dataset_dirs)))
  
        if self.shuffle:
            self._print("\nShuffling...")
            shuffle_seed = np.random.seed(42)
            if self.split:
                for s in ['train', 'val', 'test']:
                    shf = skl.shuffle(self.curr_dataset['data'][s],
                                      self.curr_dataset['labels'][s],
                                      self.curr_dataset['metadata'][s],
                                      random_state=shuffle_seed)
                    
                    self.curr_dataset['data'][s] = shf[0]
                    self.curr_dataset['labels'][s] = shf[1]
                    self.curr_dataset['metadata'][s] = shf[2]

            else:
                s = skl.shuffle(self.curr_dataset['data'],
                                self.curr_dataset['labels'],
                                self.curr_dataset['metadata'],               
                                random_state=shuffle_seed)
                self.curr_dataset['data'] = s[0]
                self.curr_dataset['labels'] = s[1]
                self.curr_dataset['metadata'] = s[2]        
  
        self.describe_dataset()
        self.clear_curr_dataset(with_split=False)
        return dataset
               
        
    def _load_dataset(self, dataset_dir):
        '''
        Entrypoint that calls methods to delegate (naively based on the dataset name)
        loading, splitting and shuffling to the correct methods for a given dataset
        '''
        if 'sunnybrook' in dataset_dir:
            self._load_sunnybrook()
        if 'acdc' in dataset_dir:
            self._load_acdc()
        elif 'mice' in dataset_dir:
            self._load_mice()
         
        if self.split is not None:
            self.train_test_split()
        
        self.describe_dataset()
        return self.curr_dataset

        
    def describe_dataset(self):
        """ Confirms data looks okay by looking for inconsistencies in sizes
        """
        
        keys = self.curr_dataset.keys()
        if 'name' in keys: 
            self._print('\nLoaded {} into dictionary:'.format(self.curr_dataset['name']))
            keys.remove('name')
                        
        if isinstance(self.curr_dataset[keys[0]], dict):

            if self.verbose:
                for key in keys:
                    for subkey in self.curr_dataset[key]:
                        print('dataset[' + key + '][' + subkey + '] --', len(self.curr_dataset[key][subkey]))
                    
            lens = [len(self.curr_dataset[k]['train']) for k in keys]
            lens += [len(self.curr_dataset[k]['val']) for k in keys]
            lens += [len(self.curr_dataset[k]['test']) for k in keys]
            lens = np.unique(lens)
            assert len(lens) == 3, 'Found inconsistent dataset sizes: {}'.format(lens)
            
            self._print('Dataset contains {} samples'.format(sum(lens)))
            self._print('split is {}'.format(lens))

        else:
            if self.verbose:
                for key in keys:
                    print('dataset[' + key + ']')

            lens = np.unique([len(self.curr_dataset[k]) for k in keys])
            assert len(lens) == 1, 'Found inconsistent dataset sizes: {}'.format(lens)
            
            self._print('Dataset contains {} samples'.format(sum(lens)))
            self._print('split is {}'.format(lens))
        return keys
        
    
    def _load_mice(self):
        '''
        Takes nothing, returns nothing. Populates self.curr_dataset with 
        mice data
        '''
        es_path = join(self.data_path, 'ES')
        ed_path = join(self.data_path, 'ED')
        
        self.curr_dataset['data'] += list(np.load(join(ed_path, 'images.npy')))
        self.curr_dataset['metadata'] += list(np.load(join(ed_path, 'dcm.npy')))
        self.curr_dataset['labels'] += list(np.load(join(ed_path, 'masks.npy')))

        self.curr_dataset['data'] += list(np.load(join(es_path, 'images.npy')))
        self.curr_dataset['metadata'] += list(np.load(join(es_path, 'dcm.npy')))
        self.curr_dataset['labels'] += list(np.load(join(es_path, 'masks.npy')))

        
    def _load_sunnybrook(self):
        """ Takes nothing, returns nothing. Populates self.curr_dataset with 
        sunnybrook data
        """       
        train_path = join(self.data_path,'train')
        val_path = join(self.data_path,'val')
        test_path = join(self.data_path,'test')
        
        self.curr_dataset['data'] += list(np.load(join(train_path, 'images.npy')))
        self.curr_dataset['metadata'] += list(np.load(join(train_path, 'dcm.npy')))
        self.curr_dataset['labels'] += list(np.load(join(train_path, 'masks.npy')))

        self.curr_dataset['data'] += list(np.load(join(val_path, 'images.npy')))
        self.curr_dataset['metadata'] += list(np.load(join(val_path, 'dcm.npy')))
        self.curr_dataset['labels'] += list(np.load(join(val_path, 'masks.npy')))

        self.curr_dataset['data'] += list(np.load(join(test_path, 'images.npy')))
        self.curr_dataset['metadata'] += list(np.load(join(test_path, 'dcm.npy')))
        self.curr_dataset['labels'] += list(np.load(join(test_path, 'masks.npy')))
        
        
    def _load_acdc(self):
        """Takes nothing, returns nothing. Populates self.curr_dataset with 
        acdc data
        """        
        self.clear_curr_dataset(with_split=True)
        train_path = join(self.data_path,'train')
        val_path = join(self.data_path,'val')
        test_path = join(self.data_path,'test')
        
        self.curr_dataset['data']['train'] = list(np.load(join(train_path, 'images.npy')))
        self.curr_dataset['metadata']['train'] = list(np.load(join(train_path, 'filenames.npy')))
        self.curr_dataset['labels']['train'] = list(np.load(join(train_path, 'masks.npy')))

        self.curr_dataset['data']['val'] = list(np.load(join(val_path, 'images.npy')))
        self.curr_dataset['metadata']['val']  = list(np.load(join(val_path, 'filenames.npy')))
        self.curr_dataset['labels']['val'] = list(np.load(join(val_path, 'masks.npy')))

        self.curr_dataset['data']['test'] = list(np.load(join(test_path, 'images.npy')))
        self.curr_dataset['metadata']['test'] = list(np.load(join(test_path, 'filenames.npy')))
        self.curr_dataset['labels']['test']  = list(np.load(join(test_path, 'masks.npy')))
        
        
    def train_test_split(self):
        """
        Returns:
            Nothing, but splits self.dataset
        """
        
        if 'acdc' in self.curr_dataset:
            # acdc already split, stratified w.r.t. pathologies/patients
            return
        
        if self.stratified:
            if 'sunnybrook' in self.curr_dataset['name']:
                self._stratified_split_sunnybrook()
            elif 'mice' in self.curr_dataset['name']:
                self._stratified_split_mice()
        
        else: 
            self._vanilla_split()
            
            
            
    def _vanilla_split(self):
        """ A regular ol' split """
        
        X = self.curr_dataset['data']
        Y = self.curr_dataset['labels']
        meta = self.curr_dataset['metadata']
        
        if self.shuffle:
            shuffle_seed = np.random.seed(123)
            X, Y, meta = skl.shuffle(X, Y, meta, random_state=shuffle_seed)

        self.clear_curr_dataset(with_split=True)
        
        train_cutoff = int(len(X) * self.split[0])
        val_cutoff = train_cutoff + int(len(X) * self.split[0])

        self.curr_dataset['data']['train'] = np.array(X[:train_cutoff])
        self.curr_dataset['labels']['train'] =  np.array(Y[:train_cutoff])
        self.curr_dataset['metadata']['train'] = np.array(meta[:train_cutoff])  

        self.curr_dataset['data']['val'] = np.array(X[train_cutoff:val_cutoff])
        self.curr_dataset['labels']['val'] =  np.array(Y[train_cutoff:val_cutoff])
        self.curr_dataset['metadata']['val'] = np.array(meta[train_cutoff:val_cutoff])  
        
        self.curr_dataset['data']['test'] = np.array(X[val_cutoff:])
        self.curr_dataset['labels']['test'] = np.array(Y[val_cutoff:])
        self.curr_dataset['metadata']['test'] = np.array(meta[val_cutoff:])



    def _stratified_split_mice(self):

        X = self.curr_dataset['data']
        Y = self.curr_dataset['labels']
        meta = self.curr_dataset['metadata']      
        self.clear_curr_dataset(with_split=True)

                
        grouped_samples = defaultdict(list) # key is study id, value is list of samples
        sample = lambda i,l,d: {'image': i, 'label': l, 'dcm': d}
        for i, l, d in zip(X, Y, meta):
            grouped_samples[d.StudyID].append(sample(i,l,d))

        study_ids = [s_id for s_id in grouped_samples.keys()]
        sample_lists = [[ds for ds in grouped_samples[s_id]] for s_id in study_ids]                        
        
        # process (all images from) one patient at a time to build up train/test split
        train_cutoff = int(len(X) * self.split[0])
        val_cutoff = int(len(X) * self.split[1])
        print('train and val cuttoffs: ',train_cutoff, val_cutoff)
        print('total: ', len(X))
        for sample_list in sample_lists:  
            if len(self.curr_dataset['data']['train']) <= train_cutoff:
                self.curr_dataset['data']['train'] += [s['image'] for s in sample_list]
                self.curr_dataset['labels']['train'] += [s['label'] for s in sample_list]
                self.curr_dataset['metadata']['train'] += [s['dcm'] for s in sample_list]
                
            elif len(self.curr_dataset['data']['train']) > train_cutoff and \
                 len(self.curr_dataset['data']['val']) <= val_cutoff:
                self.curr_dataset['data']['val'] += [s['image'] for s in sample_list]
                self.curr_dataset['labels']['val'] += [s['label'] for s in sample_list]
                self.curr_dataset['metadata']['val'] += [s['dcm'] for s in sample_list]  
                
            else:
                self.curr_dataset['data']['test'] += [s['image'] for s in sample_list]
                self.curr_dataset['labels']['test'] += [s['label'] for s in sample_list]
                self.curr_dataset['metadata']['test'] += [s['dcm'] for s in sample_list]     
  
            
    def _stratified_split_sunnybrook(self):
        """ These stratified samples keep all images for a given patient in either train or test
        set. Also, there's a equal balance across test and train sets of each pathology.

        Pathologies are stored in a dictionary, their keys are used as associated integer 
        values for referencing.

            0: SC-N (healthy)
            1: SC-HF-I (heart failure with infarction)
            2: SC-HF-NI (heart failure with no infarction)
            3: SC-HYP (LV hypertrophy)

        counts: {0: 142, 1: 245, 2: 234, 3: 184}         
        
        TODO: Clean this up
        """
        
        def pathology_name(dcm):        
            p = dcm.PatientsName.split('-') 
            return p[1] + p[2] if p[1] == 'HF' else p[1]

        def patient_name(dcm):        
            p = dcm.PatientsName.split('-') 
            return p[1] + p[2] + p[3] if p[1] == 'HF' else p[1] + p[2]

        pathology_dict = {'N': 0,'HFI': 1,'HFNI': 2,'HYP': 3}
        pathology_counts = {0: 142, 1: 245, 2: 234, 3: 184} # hack
        get_pathology = lambda dcm: pathology_dict[pathology_name(dcm)]

        num_pathologies = len(pathology_dict)
        by_pathology = {i: defaultdict(list) for i in range(num_pathologies)}

        images = self.curr_dataset['data']
        labels = self.curr_dataset['labels']
        dcm = self.curr_dataset['metadata']        
        
        self.clear_curr_dataset(with_split=True)
        
        # organize data by pathology of patient for stratified split
        for i, l, d in zip(images, labels, dcm):
            sample = {'image': i,'label': l, 'dcm': d}
            by_pathology[get_pathology(d)][patient_name(d)].append(sample)


        train_cutoff, val_cutoff = 0, 0      
        for path in by_pathology:
            #cutoff = sum([len(by_pathology[path][p]) for p in by_pathology[path]]) * self.split[0]
            train_cutoff += int(pathology_counts[path] * self.split[0])
            val_cutoff += int(pathology_counts[path] * self.split[1])

            for patient in by_pathology[path]:   
                
                if len(self.curr_dataset['data']['train']) < train_cutoff:
                    for sample in by_pathology[path][patient]:
                        self.curr_dataset['data']['train'].append(sample['image'])
                        self.curr_dataset['labels']['train'].append(sample['label'])
                        self.curr_dataset['metadata']['train'].append(sample['dcm'])
                        
                elif len(self.curr_dataset['data']['train']) >= train_cutoff and \
                     len(self.curr_dataset['data']['val']) < val_cutoff:

                    for sample in by_pathology[path][patient]:
                        self.curr_dataset['data']['val'].append(sample['image'])
                        self.curr_dataset['labels']['val'].append(sample['label'])
                        self.curr_dataset['metadata']['val'].append(sample['dcm'])  

                else: 
                    for sample in by_pathology[path][patient]:
                        self.curr_dataset['data']['test'].append(sample['image'])
                        self.curr_dataset['labels']['test'].append(sample['label'])
                        self.curr_dataset['metadata']['test'].append(sample['dcm'])  
        
    def clear_curr_dataset(self, with_split=False):

        if with_split:
            self.curr_dataset['data']  = {'train': [], 'val': [], 'test': []}
            self.curr_dataset['labels']  = {'train': [], 'val': [], 'test': []}
            self.curr_dataset['metadata']  = {'train': [], 'val': [], 'test': []}
        else:
            self.curr_dataset = {'data': [],
                                 'labels': [],
                                 'metadata': []}


    def _print(self, s):
        if self.verbose:
            print(s)
