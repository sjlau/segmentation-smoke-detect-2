import argparse
import os, sys, shutil
import numpy as np 
import time
from estimator import *
from config import *
sys.path.insert(0,'../models/')
from network import *
from network_ops import *
from train_utils import device_config

if __name__ == '__main__':
    print('transfer learning!')
    parser = argparse.ArgumentParser(description='Process session config')
    parser.add_argument('--data_paths', metavar='data_path', type=str, nargs='+',
                        help='path to input data (hdf5_files)',required=True)
    parser.add_argument('--output_dir', metavar='output_dir', type=str, nargs='?',
                        help='output directory for trained model',default='../../trained_models/all_mice')

    parser.add_argument('--run_name', metavar='run_name', type=str, nargs='?',
                        help='session name (same name as run_rame of pretrained model',required=True)
    
    parser.add_argument('--learning_rate', metavar='learning_rate', type=float, nargs='?',
                        help='learning rate for train', default=1e-3)

    parser.add_argument('--optimize', metavar='metrics_to_optimize_on', type=str, nargs='*',
                        help='loss metrics to optimize on', default=['Total_loss'])
    
    parser.add_argument('--load_model_dir_path', metavar='load_model_dir_path', type=str, nargs='?',
                        help='model directory in which pretrained model is stored',required=True)
    
    args = parser.parse_args()
    
    print('--data_paths',args.data_paths)
    print('--load_model_dir_path',args.load_model_dir_path)

    conf = conf(data_paths=args.data_paths,output_dir=args.output_dir,run_name=args.run_name,learning_rate=args.learning_rate,resume_training=True,load_model_dir_path=args.load_model_dir_path)
    
    inputs = tf.placeholder(tf.float32, shape=(None, None, None, conf.num_channels))
    targets = tf.placeholder(tf.uint8, shape = (None, None, None))
    weight_maps = tf.placeholder(tf.float32, shape=[None, None, None])
    batch_class_weights = tf.placeholder(tf.float32)
    
    print('Defining the network', conf.run_name)
model = FCMultiScaleResidualDenseNet(inputs,
                            targets, 
                            weight_maps,
                            batch_class_weights,
                            num_class=conf.num_class,
                            n_pool = 3, 
                            n_feat_first_layer = [16, 16, 16], 
                            growth_rate = 16,
                            n_layers_per_block = [2, 3, 4, 5, 4, 3, 2], 
                            weight_decay = 5e-6, 
                            dropout_rate = 0.2, 
                            optimizer = AdamOptimizer(conf.learning_rate),
                            metrics_list = ['sW_CE_loss', 'mBW_Dice_loss', 'L2_loss', 'Total_loss', 'avgDice_score',
                                            'Dice_class_1','Simple_loss'],#, 'Dice_class_2', 'Dice_class_3'],
                            #metrics_list = ['Total_loss'],#,'Dice_class_1'],
                            #metrics_to_optimize_on = ['Total_loss']
                            metrics_to_optimize_on = args.optimize
                          )

print('Preparing the estimator..')
trainer = Estimator(model = model,
                    conf = conf
                    )

total_start = time.time()
# iterate for the number of epochs
min_val_loss=2000
early_stopping_count=0
for epoch in range(int(trainer.numKeeper.counts['epoch']+1), conf.num_epochs):
    start = time.time()
    print('\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
    print('Training @ Epoch : ' + str(epoch))
    trainer.Fit(steps=-1)
    print('\n---------------------------------------------------')
    print('Validating @ Epoch : ' + str(epoch))
    loss = trainer.Evaluate(steps=-1 )

    # early stopping
    if loss>min_val_loss:
        early_stopping_count+=1
        if early_stopping_count>=15:
            print('EARLY STOPPING')
            print(min_val_loss)
            break
    else:
        min_val_loss=loss
        early_stopping_count=0
        trainer.SaveModel(os.path.join(conf.output_dir,conf.run_name,'best_model_class1','latest.ckpt'))

    trainer.numKeeper.counts['epoch'] = trainer.numKeeper.counts['epoch'] + 1
    trainer.numKeeper.UpdateCounts(trainer.summary_manager.counts)
    trainer.SaveModel(os.path.join(conf.output_dir,conf.run_name,'models','latest.ckpt'))
    print('time for Epoch ' + str(epoch) + ' : ' + str(time.time()-start))


print('total time : ' + str(time.time()-total_start))
    