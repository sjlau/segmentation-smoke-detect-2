#!/usr/bin/env python2.7
import cv2
import numpy as np
import matplotlib.pyplot as plt
from copy import copy, deepcopy
from keras import backend as K
#import seaborn as sns
#from keras.losses import binary_crossentropy
#from keras.layers import Lambda
from tensorflow.python.client import device_lib
from tensorflow import ConfigProto
from tensorflow import Session 
from os import environ
from datetime import datetime

DEFAULT_POS = 1.


def display_pred_comparison(truth, pred, img, dcm, pos_class=1, verbose=False, save=False):
    
    metrics, truth_vs_pred_img = compare_mask_prediction(truth, pred, vis=False, pos=pos_class)

    overlaid_contour_img = overlay_contour(img, truth, vis=False, pos=pos_class)
    overlaid_pred_img = overlay_contour(img, pred, vis=False, pos=pos_class)
    
    fig, axs = plt.subplots(1,3,figsize=(20,7))
    
    if verbose:
        title = "Dice Score: {}%\nStudyID: {}; SeriesNumber: {}; InstanceNumber: {}"                                    
        fig.suptitle(title.format(metrics['dice'], 
                                  dcm.StudyID, 
                                  dcm.SeriesNumber, 
                                  dcm.InstanceNumber), 
                     fontsize=20)  
    truth_vs_pred_img = np.array(truth_vs_pred_img).reshape((img.shape[0],img.shape[1],3))

    axs[0].imshow(truth_vs_pred_img, cmap = 'gray')
    axs[0].set_title('Prediction vs. Truth', fontsize=20)

    axs[1].imshow(overlaid_pred_img, cmap = 'gray')
    axs[1].set_title('Prediction', fontsize=20)

    axs[2].imshow(overlaid_contour_img, cmap = 'gray')
    axs[2].set_title('Expert Contour', fontsize=20)
    
    if save:
        fig.savefig(fname, dpi=1000, facecolor='w', edgecolor='w', format='png',
                    transparent=False, bbox_inches=None, pad_inches=0.1)
        
    return truth_vs_pred_img, overlaid_pred_img, overlaid_contour_img
        
        
def overlay_contour(image, contour, pos=DEFAULT_POS, logits=False, 
                    vis=True, size=(6,6)):
    """ Overlays a contour onto an image.
    Parameters:
        image   -- The frame corresponding to the contour
        contour -- The contour to overlay on the image
        pos     -- Value representing a positive sample, i.e., the value
                   that pixels representing the contour take on (1 or 0)
        vis     -- Set to false to return the overlaid image instead of displaying
        size    -- Size of the matplotlib figure to display

    Returns:
        Nothing if vis is True, the overlaid image if vis is False.
    """
    
    image = image[:,:,0] if len(image.shape) > 2 else image
    contour = contour[:,:,0] if len(contour.shape) > 2 else contour
    
    dcm = deepcopy(image)
    dcm = np.stack((dcm,)*3, -1)
    
    if logits:
        green_multiplier = logits * [0, 1, 0]
    else:
        green_multiplier = [0, 1, 0]
    mask = deepcopy(contour)
    mask = np.array([[True if c == pos else False for c in row] for row in mask])
    dcm[mask, :] *= green_multiplier

    if vis:
        # if vis is true, display here
        plt.figure(figsize=size)
        plt.imshow(dcm)
    else:
        # If not, return the overlaid image
        return dcm
    

    
def compare_mask_prediction(truth, pred, vis=True, pos=DEFAULT_POS):
    """ Function to calculate the pixel-wise true positives, true negatives, 
    false positives, and false negatives in one prediction. If vis=True,
    it will display an image where... 
    Green: true positives
    Red:   false positives
    Blue:  false negatives
    White: true negatives
    """

    pos, neg = (0.,1.) if pos == 0. else (1.,0.)
    if len(truth.shape) > 2: truth = truth[:,:,0]
    if len(pred.shape) > 2: pred = pred[:,:,0]
        
    tp, fp, tn, fn = 0., 0., 0., 0.  
    img = np.zeros((truth.shape[0],truth.shape[1],3))
    
    for i, (row_truth, row_pred) in enumerate(zip(truth, pred)):
        row_truth, row_pred = row_truth.tolist(), row_pred.tolist() 

        row = np.zeros((truth.shape[0],3))
        for j, (pixel_truth, pixel_pred) in enumerate(zip(row_truth, row_pred)):

            if neg == pixel_truth == pixel_pred:
                tn += 1.
                row[j, :] = [1., 1., 1.]
            elif neg == pixel_truth and pos == pixel_pred:
                fp +=1.
                row[j, :] = [1., 0., 0.]
            elif pos == pixel_truth and neg == pixel_pred:
                fn += 1.
                row[j, :] = [0., 0., 1.]
            elif pos == pixel_truth == pixel_pred:
                tp += 1.
                row[j, :] = [0., 1., 0.]
        img[i] = row

    dice = (2*tp)/(2*tp + fp + fn + 0.001)*100

    if vis:
        display_str = "true pos: {}; false pos: {}; true neg: {}; false neg: {}"
        print(display_str.format(tp, fp, tn, fn))
        print("dice coeff: {}".format(dice))
        
        plt.figure(figsize=(6,6))
        plt.imshow(img)
    
    metrics = {'tp': tp, 'fp': fp, 'tn': tn, 'fn': fn, 'dice': dice}
    return metrics, img


def evaluate_predictions(predictions, contours, pos=DEFAULT_POS):
    """ Iterates through a set of predictions and ground truths, totals up
    true pos, false pos etc., and calculates precision, recall, f-1 
    and dice scores.
    """
    dice_scores = []
    
    true_pos, false_pos, true_neg, false_neg = [], [], [], []
    for pred, contour in zip(predictions, contours):

        metrics, _ = compare_mask_prediction(contour, pred, False, pos=pos)
        
        dice_scores.append(metrics['dice'])
        true_pos.append(metrics['tp'])
        false_pos.append(metrics['fp'])
        true_neg.append(metrics['tn'])
        false_neg.append(metrics['fn'])
        
    total_tp = float(sum(true_pos)) 
    total_fp = float(sum(false_pos))
    total_tn = float(sum(true_neg)) 
    total_fn = float(sum(false_neg)) 
    prec = total_tp / (total_tp + total_fp + 0.001)
    rec = total_tp / (total_tp + total_fn + 0.001)
    
    d = (2*total_tp)/(2*total_tp + total_fp + total_fn + 0.001)*100

    print("\tprecision:", prec)
    print("\trecall:", rec)
    print("\tdice:", d)
    print("tp, fp, tn, fn",total_tp,total_fp,total_tn,total_fn)
    return {'tp': total_tp, 
            'fp': total_fp, 
            'tn': total_tn, 
            'fn': total_fn,
            'prec': prec,
            'rec': rec,
            'dice': d,
            'dice_list': dice_scores}


def evaluate_model(model, X, Y, pos=DEFAULT_POS):

    preds = model.predict(X)
    preds = np.where(preds > 0.5, 1, 0)
    metrics = evaluate_predictions(preds, Y, pos=pos)
    return preds, metrics


def sort_on_predictions(dice_scores, meta, X, Y, preds, probs):
    """ Sorts the dice scores, metadata, images, labels  and predictions
    in parallel based on the dice scores. -1 is best, 0 is worst.
    """
    return dict(zip(['dice_list', 'meta', 'X', 'Y', 'preds', 'probs'],
                 zip(*sorted(zip(dice_scores, meta, X, Y, preds, probs)))))


def print_metric_averages(metrics, exp_name):
    
    precision, recall, dice, epochs = [], [], [], []
    for run in metrics:
        m = run[exp_name]

        if 'epochs_trained' in m:
            if m['epochs_trained'] != 0 and m['dice'] != 0:
                epochs += [m['epochs_trained']]        
            #print '\n\nEpochs', m['epochs_trained']
    
        print('Precision', m['prec'])
        print('Recall', m['rec'])
        print('Dice', m['dice'], '\n')
        
        if m['prec'] != 0: precision += [m['prec']]
        if m['rec'] != 0: recall += [m['rec']]
        if m['dice'] != 0: dice += [m['dice']]
    print(len(epochs), len(precision), len(recall), len(dice))
    print(s.format('convergence epochs', np.mean(epochs), np.std(epochs)).replace('%',''))
    print(s.format('precision', np.mean(precision)*100, np.std(precision)*100))
    print(s.format('recall', np.mean(recall)*100, np.std(recall)*100))
    print(s.format('dice', np.mean(dice), np.std(dice)))

def plot_dice(test_dice, train_dice=None, bins=25, save_path=None, figsize=(8,5)):
    
    # shut up stats.py
    import warnings; warnings.simplefilter('ignore')
    
    plt.figure(figsize=figsize)
    
    title = 'Test Set Dice Score Frequency Distribution'
    if train_dice is not None: 
        title = 'Train and ' + title
        
    plt.title(title, fontsize=18) 
    
    plt.xlim(0, 100)
    plt.xlabel('Dice Scores ({} bins)'.format(bins), fontsize=14)
    
    ax = sns.distplot(test_dice, bins=bins, color='blue', label='test')
    if train_dice is not None:
        ax = sns.distplot(train_dice, bins=bins, color='red', label='train')

    if isinstance(save_path, str):
        plt.savefig(save_path, dpi=1000, facecolor='w', edgecolor='w', format='png',
                    transparent=False, bbox_inches=None, pad_inches=0.1)
    plt.show()
        