from __future__ import division
import numpy as np
import os, shutil, sys
import SimpleITK as sitk
import glob
from datetime import datetime
import time
import re, nibabel
import pandas as pd

from config import *
from test_utils import *


sys.path.insert(0,'../models/')
from network import *
from network_ops import *


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process session config')
    parser.add_argument('--data_paths', metavar='data_path', type=str, nargs='+',
                        help='path to input data (hdf5_files)',required=True)
    parser.add_argument('--output_dir', metavar='output_dir', type=str, nargs='?',
                        help='output directory for trained model',required=True)

    parser.add_argument('--run_name', metavar='run_name', type=str, nargs='?',
                        help='session name',required=True)
    
    #just to work with train.slrm (so commandline arguments dont get rejected...)
    parser.add_argument('--learning_rate', metavar='learning_rate', type=float, nargs='?',
                        help='learning rate for train', default=1e-3)
    parser.add_argument('--optimize', metavar='metrics_to_optimize_on', type=str, nargs='*',
                        help='loss metrics to optimize on', default=['Total_loss'])
    parser.add_argument('--batch_size', metavar='batch_size', type=int, nargs='?',
                        help='train batch size', default=8)
    
    args = parser.parse_args()

    conf = conf(data_paths=args.data_paths,output_dir=args.output_dir,run_name=args.run_name)

    conf.load_pretrained_model_from = os.path.join(conf.output_dir,conf.run_name,'best_model_class1','latest.ckpt')
    
    save_dir = os.path.join(conf.output_dir, conf.run_name, 'predictions{}'.format(time.strftime("%Y%m%d_%H%M%S")))
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    os.makedirs(save_dir)

    models = ['best_model_class1/latest.ckpt']

    for model in models:   
        saved_model_dir = os.path.join(save_dir, model.split('/')[0]) 
        os.makedirs(saved_model_dir)
        model_path = os.path.join(conf.output_dir, conf.run_name, model)
        
        gt_available = True
        final_test_data_path = ['../../processed_acdc_dataset/dataset/test_set']
        #                         '../../processed_acdc_dataset/dataset/validation_set']
        
        inputs = tf.placeholder(tf.float32, shape=(None, None, None, conf.num_channels))
        targets = tf.placeholder(tf.uint8, shape = (None, None, None))
        weight_maps = tf.placeholder(tf.float32, shape=[None, None, None])
        batch_class_weights = tf.placeholder(tf.float32)


        # define the network
        print('Defining the network', conf.run_name)
        model = FCMultiScaleResidualDenseNet(inputs,
                            targets, 
                            weight_maps,
                            batch_class_weights,
                            num_class=conf.num_class,
                            n_pool = 3, 
                            n_feat_first_layer = [16, 16, 16], 
                            growth_rate = 16,
                            n_layers_per_block = [2, 3, 4, 5, 4, 3, 2], 
                            weight_decay = 5e-6, 
                            dropout_rate = 0.2, 
                            optimizer = AdamOptimizer(conf.learning_rate),
                            metrics_list = ['sW_CE_loss', 'mBW_Dice_loss', 'L2_loss', 'Total_loss', 'avgDice_score',
                                            'Dice_class_1'],#, 'Dice_class_2', 'Dice_class_3'],
                            #metrics_list = ['Total_loss'],#,'Dice_class_1'],
                            metrics_to_optimize_on = ['Total_loss']
                          )
        
        # initialise the estimator with the net
        print('Preparing the Tester..')
        tester = Tester(model, conf, model_path)
        results = []
        patients = []
        
        print("-----------------------------------------------------------------------\n")
        i_time = time.time()
        result = tester.LoadAndPredict(None, None, None, outputs_shape = None,
                                #preProcessList = ['roi_detect', 'normalize', 'roi_crop'],
                                preProcessList = [],
                                postProcessList = ['glcc', 'glcc_2D', 'pad_patch'],
                                # preProcessList = ['crop_pad_4d', 'normalize'],
                                # postProcessList = ['glcc', 'glcc_2D'],
                                crf = None,
                                patch_size=(128, 128),
                                save_path = os.path.join(saved_model_dir,'predictions')
                                )
        print("Time taken for Prediction: " + str(time.time()-i_time) + 's')
        print("\n")
        results.append(result)
        