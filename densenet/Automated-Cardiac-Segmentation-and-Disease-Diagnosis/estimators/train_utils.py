from tensorflow import ConfigProto
from tensorflow import Session 
from os import environ
from tensorflow.python.client import device_lib
import argparse


def init_parser():
    parser = argparse.ArgumentParser(description='Process session config')
    parser.add_argument('--data_paths', metavar='data_path', type=str, nargs='+',
                        help='path to input data (hdf5_files)', required=True)
    parser.add_argument('--output_dir', metavar='output_dir', type=str, nargs='?',
                        help='output directory for trained model', required=True)

    parser.add_argument('--run_name', metavar='run_name', type=str, nargs='?',
                        help='session name', required=True)

    parser.add_argument('--learning_rate', metavar='learning_rate', type=float, nargs='?',
                        help='learning rate for train', default=1e-3)

    parser.add_argument('--optimize', metavar='metrics_to_optimize_on', type=str, nargs='*',
                        help='loss metrics to optimize on', default=['Total_loss'])
    
    parser.add_argument('--batch_size', metavar='batch_size', type=int, nargs='?',
                        help='train batch size', default=8)
    
    return parser

def check_parser_args(args):
    print('CHECK ARGS')
    print('data_paths',args.data_paths)
    print('output_dir',args.output_dir)
    print('run_name',args.run_name)
    print('learning_rate',args.learning_rate)
    print('metrics_to_optimize_on',args.optimize)
    print('batch_size',args.batch_size)

def device_config(verbose=True):
    """ Configure GPU settings. Will run on as many as are available """
    
    if verbose:
        print(device_lib.list_local_devices())
        
    n_gpus = 0
    for dev in device_lib.list_local_devices():
        if 'gpu' in dev.device_type.lower():
            n_gpus += 1

    if n_gpus > 0:
        gpu_devices = ','.join([str(i) for i in range(n_gpus)])
        environ['CUDA_VISIBLE_DEVICES'] = gpu_devices

        #config = ConfigProto()
        #config.gpu_options.allow_growth = True
        #config.gpu_options.per_process_gpu_memory_fraction = 0.4
        #session = Session(config=config)

    return n_gpus

PS_OPS = [
    'Variable', 'VariableV2', 'AutoReloadVariable', 'MutableHashTable',
    'MutableHashTableOfTensors', 'MutableDenseHashTable'
]

# see https://github.com/tensorflow/tensorflow/issues/9517
def assign_to_device(device, ps_device):
    """Returns a function to place variables on the ps_device.

    Args:
        device: Device for everything but variables
        ps_device: Device to put the variables on. Example values are /GPU:0 and /CPU:0.

    If ps_device is not set then the variables will be placed on the default device.
    The best device for shared varibles depends on the platform as well as the
    model. Start with CPU:0 and then test GPU:0 to see if there is an
    improvement.
    """
    def _assign(op):
        node_def = op if isinstance(op, tf.NodeDef) else op.node_def
        if node_def.op in PS_OPS:
            return ps_device
        else:
            return device
    return _assign

# Source:
# https://stackoverflow.com/questions/38559755/how-to-get-current-available-gpus-in-tensorflow
def get_available_gpus():
    """
        Returns a list of the identifiers of all visible GPUs.
    """
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']