# segmentation-smoke-detection
This guide assumes you have the `config` file from [title](https://nautilus.optiputer.net/) in your `.kube` folder

## Setup
### Create a deployment
Add `kerasDeployment.yaml` to your `.kube` folder. Replace all `sophia`'s with your name.
Navigate to your `.kube` folder and run: 
```bash
kubectl create -f kerasDeployment.yaml
```
To see the status of your deployment, run:
```bash
kubectl get pods
```
When the status says `Running`, you are ready to use it. This may take a few minutes.

### Shell into your deployment
```bash
kubectl exec -it <namespace> bash
```
Replace <namespace> with the name of your deployment. 

### Install packages
There are some packages required for the main notebook, unet_basics_demo:
```bash
pip install seaborn
pip install scikit-image
```

### Open Jupyter Notebook
```bash
jupyter notebook --ip=0.0.0.0 --port=8888
```
You can replace 8888 with another port number. When successful, you will get some links. Copy and paste the last link into your browser, but you won't be able to access it yet.

### Forward the port from local machine
Before you can access Jupyter Notebook, you need to forward the port. In another terminal, run:
```bash
kubectl port-forward <namespace> 8888:8888
```
Replace <namespace> with the name of your deployment. You should be able to access Jupyter Notebook from your browser now. 

## Repo Walkthrough 
- `unet/` 
    - `unet_basics_demo.ipynb` 
        - Use this to train and test a model
    - `unet_cv.ipynb`
        - Trains and tests a model using 5-fold cross validation
    - `unzipping_renaming_scripts.ipynb`
        - Some scripts used to unzip and format data 
    - `xml_reader.ipynb`
        - Parses xml label file and converts to a jpg
- `unet/data/`
    - `preprocessed/`
        - Contains grouped .npy training images and masks from  Wildfire Observers
    - `smer/preprocessed` 
        - Contains grouped .npy images and masks from HPWREN FIgLib smer
- `preprocess/`
    - `preprocess.ipynb`
        - Use this to convert .jpg to .npy and group images and masks into folders

## Datasets
1. Wildfire Observer dataset: http://wildfire.fesb.hr/index.php?option=com_content&view=article&id=49&Itemid=54 <br>
Train images: 64 <br>
Test images: 49<br>
Labeled as smoke, maybe smoke, and not smoke. Since there some false positives for "maybe smoke" regions, we only used the regions labeled "smoke" as smoke and "maybe smoke" was classfied as "not smoke".

2. HPWREN FIgLib smer dataset: http://hpwren.ucsd.edu/HPWREN-FIgLib/HPWREN-FIgLib-Data/20160604_FIRE_smer-tcs3-mobo-c/ <br>
Labels (.jpg): https://drive.google.com/drive/u/1/folders/1R5yJ63UCWB2Mi2u2r7AjGhUbLCkvKFMo <br>
Labels (.xml): https://drive.google.com/drive/u/1/folders/1cNHjl1Sl0e7g2dUGDHTtuzp2Wl15l5L2 <br>
Images: 22

## Getting Results
Run `unet_basics_demo.ipynb` to train and test a UNet. <br>
Or run `unet_cv.ipynb` train and test a UNet using cross validation. <br>
You may need to run `pip install scikit-image` and `pip install seaborn` in the terminal to resolve import errors. 

## Adding More Data
Images can be added as .jpg or .npy files. Masks can be added as .jpg or .xml files. Add new data to `unet/data/`. <br>
If adding images as .jpg files, run `preprocess.ipynb` to convert .jpg to .npy and group images and masks together. <br>
If adding masks as .xml files from HPWREN FIgLib smer, run `unet/xml_reader.ipynb` to convert .xml to .npy. <br>
In the end, the files should be organized like the following example: <br>
- `unet/data/training/`
    - `00000`
        - `00000_image.npy`
        - `00000_mask.npy`
    - `00001`
        - `00001_image.npy`
        - `00001_mask.npy`
    - etc.

## Improving the model
Possible ways to improve performance statistics: <br>
- Clean up/relabel inconsistent "maybe smoke" labels in the Wildfire Observers dataset
- Adding more data  

## Module Versions
Python: 3.6.10 <br>
Jupyter Notebook: 6.1.4 <br>
Keras: 2.4.3 <br>
Tensorflow: 2.3.0/1 <br>
numpy: 1.19.2 <br>
matplotlib: 3.3.1




        






