#!python3

import os
import pandas as pd
from datetime import datetime


class Logger:
    """ A basic, general-purpose logger using CSV files """

    def __init__(self, fpath, schema=None, caller=None):
        """ Initialize the logger
        NOTE for user: on schema change, create a new log file

        fpath -- path to the log file
        schema -- a python list of column names
            - if None, import an existing CSV log file
            - if given, init log df with schema and overwrite the file
              at fpath (if any) with the csv header (schema)
            - NOTE: does NOT expect 'datetime' and 'caller' columns
        caller -- name of the caller (which class or procedure, etc)
        read_existing -- option to read in the existing CSV at fpath
        """
        self.fpath = fpath
        self.caller = caller if caller else "NONE"
        if schema:
            schema = ['datetime', 'caller'] + schema
            self.df = pd.DataFrame(columns=schema)  # init df with given schema
            with open(fpath, 'w') as f:  # csv header
                f.write(','.join(schema) + '\n')
        elif os.path.exists(fpath):  # read an existing csv
            self.df = pd.read_csv(fpath)  # throw IOException if file not found
        else:
            raise ValueError(
                "Must create a logger instance from either a schema or an existing log file!")
        self.row = dict()  # dict as row buffer

    def get_schema(self):
        """ returns a python list of column names """
        return self.df.columns.tolist()

    def get_head(self, n=None):
        return self.df.head(n)

    def log_item(self, key, value):
        """ log one item in the current row 
        NOTE: writes to a row buffer (dict), call commit_line() when done

        key -- column/item key
        value -- value to log
        """
        if not key in self.df.columns.tolist():
            raise ValueError("Key not found!")
        self.row[key] = value

    def log_items(self, row_dict, commit=True):
        """ log multiple items with a key->value dict 
        NOTE: writes to a row buffer (dict), call commit_line() when done
        """
        for key in list(row_dict.keys()):
            self.log_item(key, row_dict[key])
        if commit:
            self.commit_line()

    def commit_line(self, write_csv=False):
        """ write one row to the internal dataframe 

        write_csv -- whether to append the current line to a csv file
        fpath -- if write_csv, specify the csv path if not self.fpath
        header -- if write_csv, whether to write the CSV header
        """
        if not "row" in dir(self):  # nothing to commit
            return
        if not "datetime" in self.row.keys():  # check if datetime logged
            self.row['datetime'] = str(datetime.now())
        if not "caller" in self.row.keys():  # check if caller logged
            self.row['caller'] = self.caller
        self.df = self.df.append(self.row, ignore_index=True)
        self.row = dict()  # clear the row dict
        if write_csv:
            with open(self.fpath, 'a') as f:
                f.write(','.join([str(x) for x in self.df.iloc[-1].tolist()]))
                f.write('\n')

    def dump_csv(self, fpath=None, append=True):
        """ dump the entire internal logger dataframe to a csv file
        NOTE: if target path doesn't exist, create

        fpath -- path to the CSV file (if different from self.fpath)
        append -- whether to append to file or overwrite
        """
        if fpath == None:
            fpath = self.fpath
        d = os.path.dirname(fpath if fpath else self.fpath)
        if not os.path.exists(d):
            os.makedirs(d)
        if append:
            self.df.to_csv(fpath, header=False, index=False, mode='a')
        else:
            self.df.to_csv(fpath, index=False, mode='w')
