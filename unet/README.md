## Contents

#### Example Notebook Overview
unet_basics 
* Partitioning patient data and using DataGenerators
* Building, training, evaluating and saving U-Net models 
* Configuring compute devices
* Visualizing results
    * loss and Dice curves, Dice score distribution, prediction/groundtruth overlays 
    
unet_transfer_learning
* Loading trained U-Net models


#### Misc. Tips
* If you're having OOM problems, try shutting down the kernels for all notebooks except the one you're running