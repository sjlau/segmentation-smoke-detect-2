"""
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.
"""
from collections import defaultdict
import numpy as np
import inspect
import cv2
from py_unet.utils.data_augmentation_fns import zoom_fn, shift_fn, shear_fn, rotate_fn, gaussian_noise_fn


class BatchAugmentor:
   

    def __init__(self, augmentation_args, batch_size, additive=False, 
                 max_additive_augs=-1, preproc_fn=None, postproc_fn=None):
        """ Class for performing probabilistic batch-wise data augmentation on images and masks 
        
        args:
            augmentation_args: A dictionary where keys are names of supported transformations
               and values are are tuples of arguments like:
               (p_aug, (arg2_lower_lim, arg_upper_lim), (arg2_lower_lim, arg2_upper_lim), ...)
               where p_aug is the probability that this augmentation will be applied, and every
               subsequent value is a tuple whose values indicate the lower and upper limits of
               the distribution from which the corresponding argument will be sampled on each 
               application of the function.
            batch_size: batch size of the DataGenerator using the instance of this class
            additive: Whether or not to apply multiple transformations to the same image
            max_additive_augs: Max number of transformations to apply to the same image
            normalize_output: True for Min-max scaling of images after transformations 
        
        """        
    
        self.batch_size = batch_size
    
        # Access with self._aug_fn()
        self._aug_fn_dict = {
            'add_gaussian_noise': gaussian_noise_fn,            
            'horizontal_flip': np.fliplr,
            'vertical_flip': np.flipud,
            'rotate': rotate_fn,
            'shear': shear_fn,
            'shift': shift_fn, 
            'zoom': zoom_fn,
        }    

        # Default augmentation probabilities and argument distribution limits 
        default_args = { 
            'add_gaussian_noise': (0.5, (0.01,0.1)),            
            'horizontal_flip':    (0.5,),
            'vertical_flip':      (0.5,),
            'rotate':             (0.5, (-45,45)),
            'shear':              (0.5, (.05, 2.)),
            'shift':              (0.5, (-0.05, 0.05), (-0.05, 0.05)),
            'zoom':               (0.5, (0.95, 1.05)),
        }  
        
        # Names of transformations that shouldn't be applied to masks
        self._transform_image_only = set(['add_gaussian_noise'])

        to_tup = lambda v: v if isinstance(v, tuple) else (v,)
        update = lambda d, k: default_args[k] if d[k]==True else to_tup(d[k])
        
        self.augmentations = {k: update(augmentation_args,k)
                              for k in augmentation_args
                              if augmentation_args[k] != False}          
                
        self.additive = additive
        self._max_additive_augs = max_additive_augs
        
        self._preproc_fn = preproc_fn
        self._postproc_fn = postproc_fn
        
        self._frame_ops_maxed = lambda frame_ops: len(frame_ops) >= self._max_additive_augs
        self._requires_args = lambda op_name: len(self.augmentations[op_name]) > 1

        
    def process_batch(self, X_batch, Y_batch=None, apply_batch_augs=None, prev_batch_augs=None):
        """ Probabilistic additive augmentation for images and their masks 
        args:
            X_batch - Batch of images with shape (batch_size, rows, cols, channels)
            Y_batch - Batch of masks with shape (batch_size, rows, cols, channels)
            identity - list of booleans for switching off augmentation, where True 
                       indicates the image and mask and at that location will be preserve                    
        """
        
        batch_size = X_batch.shape[0]
       
        X_aug = X_batch[:,:,:,0]
        Y_aug = Y_batch[:,:,:,0] if Y_batch is not None else Y_batch        

        augs_used = [None] * batch_size
        augs_all = set(list(self.augmentations.keys()))

        for i in range(batch_size): 
            
            if not self.additive:                
                op_name = None
                
                if apply_batch_augs is not None:
                    # Use augmentation specified by caller
                    op_name = apply_batch_augs[i]
               
                if op_name == 'random' or apply_batch_augs is None:
                    # Choose a random augmentation for current image in batch
                    unused = list(augs_all - set(prev_batch_augs[i]))
                    if len(unused) > 0:                                           
                        op_name = np.random.choice(unused)  

                # apply the transformation
                X_aug[i], Y_aug[i] = self._apply(op_name, X_aug[i], Y_aug[i]) 
                augs_used[i] = op_name              

            else:
                raise NotImplementedError("TODO")

        X_aug = X_aug[..., np.newaxis]
        if Y_aug is not None:
            Y_aug = np.where(Y_aug > .2, 1, 0)[..., np.newaxis]
            
        return X_aug, Y_aug, augs_used
    

    
    def _batch_apply(self, op_name, coins, X_batch, Y_batch=None):
        """ 
        Returns a data augmentation function with parameters set 
        args:
            batch_data - A Numpy matrix containing the batch of images or masks to transform
            op_name - Name of transformation to apply
            coins - A boolean list of length self.batch_size 
        """

        # Iterate over batch, transform if coin is true
        for i in range(X_batch.shape[0]):
            if coins[i] == True:
                if Y_batch is not None:
                    X_batch[i], Y_batch[i] = self._apply(op_name, X_batch[i], Y_batch[i])
                else:
                    X_batch[i], _ = self._apply(op_name, X_batch[i])
                
        return X_batch, Y_batch
    
    
    def _apply(self, op_name, X, Y=None):
        """ Transform a single image and mask """
        
        if op_name is not None:
            try:
                aug_fn = self._aug_fn_dict[op_name]

                if self._requires_args(op_name):
                    kwargs = self._compose_kwargs(op_name)
                    aug_fn = aug_fn(**kwargs)
                
                X = aug_fn(X)
                if op_name not in self._transform_image_only and Y is not None:
                    Y = aug_fn(Y) 

            except Exception as err:
                err_str = "BatchTransformer: Error applying augmentation function {}"
                print(err_str.format(op_name))
                print("\nCaught exception:\n{}".format(err))
            
        return X, Y
       
    
    
    def _compose_kwargs(self, op_name):
        """" Prepare arguments for transformations that require them """
  
        kwargs = {}  

        arg_names = inspect.getfullargspec(self._aug_fn_dict[op_name])[0]
        arg_limits = self.augmentations[op_name][1:] # first arg is probability
                
        for i, arg_name in enumerate(arg_names):
            kwargs[arg_name] = np.random.uniform(arg_limits[i][0], 
                                                 arg_limits[i][1])
           
        return kwargs
    
    

        
        
        
