"""
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.
"""
import tensorflow as tf
from tensorflow.keras.models import *
from tensorflow.keras.layers import Input, concatenate, Conv2D, MaxPooling2D, UpSampling2D, Dropout, Cropping2D, Activation, BatchNormalization, ZeroPadding2D
from tensorflow.keras.layers import Conv3D, MaxPooling3D, UpSampling3D, Cropping3D,  ZeroPadding3D, Lambda
from tensorflow.keras.initializers import Orthogonal, glorot_normal
from tensorflow.keras.optimizers import *
from tensorflow.keras.regularizers import l2
from tensorflow.python.client import timeline
from tensorflow.keras import backend as K
import os

from py_unet import ModelMGPU


class UnetBuilder:
    
    """ Class for building a U-Net with variable depth and conv filters
    args:
        input_dim: if data_format='channels_last' (default format): (rows, cols, channels) 
                   if data_format='channels_first': (channels, rows, cols)                        
        ds_filter_counts: Number of convolutional filters to use for layers in each block
                   of the U-net's contracting bath. For example, the default value of 
                   [8, 16, 32, 64, 128]  will build a U-Net with 9 blocks, where 
                   ds_filter_counts[:-1] corresponds to a 4 block contracting path,
                   ds_filter_counts[-1] corresponds to the bottleneck block, and 
                   ds_filter_counts[:-1] reversed corresponds to a 4 block expanding path.
    """
               
    def __init__(self, 
                 input_dim, 
                 ds_filter_counts=[8, 16, 32, 64, 128],
                 kernel_size=(3,3),
                 kernel_initializer=glorot_normal,
                 kernel_regularizer=l2,
                 kernel_init_seed=None,
                 pool_size=(2,2), 
                 batch_norm=False, 
                 dropout=False, 
                 weight_decay=1e-4,
                 n_gpus=1, 
                 output_activation='sigmoid'):
        
        self.input_dim = input_dim
        self.ds_filter_counts = ds_filter_counts
        
        self.kernel_size = kernel_size
        self.pool_size = pool_size
        
        self.kernel_init_seed = kernel_init_seed
        self.weight_decay =  weight_decay

        self.kernel_init = kernel_initializer             
        if self.kernel_init is not None:
            self.kernel_init = self.kernel_init(seed=self.kernel_init_seed) 
            
        self.kernel_reg = kernel_regularizer
        if self.kernel_reg is not None:
            self.kernel_reg = self.kernel_reg(self.weight_decay)              

        self.batch_norm = batch_norm
        self.dropout = dropout        
        self.weight_decay = weight_decay
        self.n_gpus = n_gpus
        print('before:', self.n_gpus)
        self.output_activation = output_activation     

                 
    def build(self):
       
        filters = self.ds_filter_counts
        n_filters = len(filters)
        self._n_blocks = (n_filters*2) - 1
       
        inputs = Input(self.input_dim)
        
        # Contracting path
        levels = [ self._conv_block(filters[0]) (inputs) ]  
        ds = [ MaxPooling2D(pool_size=self.pool_size) (levels[-1]) ]
        
        for i in range(1, n_filters-1):
            levels += [ self._conv_block(filters[i]) (ds[-1]) ]   
            ds += [ MaxPooling2D(pool_size=self.pool_size) (levels[-1]) ]            
            
        # Bottleneck block
        levels += [ self._conv_block(filters[-1]) (ds[-1]) ]

        # Expanding path
        us_block = UpSampling2D(size=self.pool_size)(levels[-1])
        us_block = self._conv_block(filters[-2], 1, (2,2), False) (us_block)
        us =  [ concatenate([us_block, levels[-2]],  axis=3) ]
        
        for i in range(0, n_filters-2):
            
            levels += [ self._conv_block(filters[-2-i]) (us[-1]) ]          
            us_block = UpSampling2D(size=self.pool_size)(levels[-1])
            us_block = self._conv_block(filters[-2], 1, (2,2), False) (us_block)     
        
            try: 
                us += [ concatenate([us_block, levels[2-i]],  axis=3) ]
      
            except:
                padding = self._get_padding(K.int_shape(levels[2-i]))
                us += [ concatenate([ZeroPadding2D(padding) (us_block),
                                     levels[2-i]], 
                                    axis=3) ]

        # Final block
        levels += [ self._conv_block(filters[0]) (us[-1]) ] 
        outputs = self._conv_act(1, (1,1), False, self.output_activation) (levels[-1])
        
        model = Model(inputs=inputs, outputs=outputs)
        
        print(self.n_gpus)
        if self.n_gpus > 1:
            return ModelMGPU(model, self.n_gpus) 
        else:
            return model

        
    def _conv_act(self, n_filters, kernel_size, batch_norm=False, act='relu'):
        """ Creates a conv2d layer with ReLU activation (with optional batchnorm) """
        
        def f(input_layer):

            conv = Conv2D(filters=n_filters,
                          kernel_size=kernel_size,
                          strides=(1,1),
                          padding='same',
                          use_bias=False,
                          kernel_initializer=self.kernel_init,
                          kernel_regularizer=self.kernel_reg,
                          bias_regularizer=self.kernel_reg,)(input_layer)
           
            
            if batch_norm: 
                conv = BatchNormalization()(conv)
                
            if act is not None:
                return Activation(act) (conv)
            else:
                return conv
            
        return f


    def _conv_block(self, n_filters, depth=2, kernel_size=None, batch_norm=None, act='relu'):
        """ For stacking `depth` number of self._conv_act """
        size = self.kernel_size if kernel_size is None else kernel_size
        bn = self.batch_norm if batch_norm is None else batch_norm
        
        def f(input_layer):
            inputs = [input_layer]
            for i in range(depth):
                act_layer = self._conv_act(n_filters, size, bn, act)(inputs[i])
                if self.dropout and depth > 1 and i+1 < depth:
                    act_layer = Dropout(0.25) (act_layer)
                inputs += [act_layer]
            return inputs[-1]

        return f



    def _get_padding(self, shape):
        x_crop = 1 if shape[1] % 2 == 1 else 0
        y_crop = 1 if shape[2] % 2 == 1 else 0
        return ((0, x_crop), (0, y_crop))
    

