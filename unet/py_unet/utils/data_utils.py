"""
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.
"""
import os


def get_split_dirs(data_dirs, create_patient_split=True, split=[0.75, 0.15, 0.10], 
                   randomize_split=False, **kwargs):
    
    if create_patient_split:
        return split_patient_directories(data_dirs, split, randomize_split, **kwargs)
    else:
        train_dirs, val_dirs, test_dirs = [], [], []
        for data_dir in data_dirs:
            train_dirs.append(os.path.join(data_dir, 'train'))
            val_dirs.append(os.path.join(data_dir, 'val')) 
            test_dirs.append(os.path.join(data_dir, 'test'))
            
        count = lambda dirs: sum([len(os.listdir(d))/2 for d in dirs])
        train_count = count(train_dirs)
        val_count = count(val_dirs)
        test_count = count(test_dirs)
        N = train_count + val_count + test_count
        s = '{} samples for {} ({}% of {})'
        print(s.format(train_count, 'training', train_count / N, N)) 
        print(s.format(val_count, 'validation', val_count / N, N)) 
        print(s.format(test_count, 'testing', test_count / N, N))             
        return train_dirs, val_dirs, test_dirs 
    

def split_patient_directories(data_dirs, split, randomize=False, test_holdout=None, counts=False):
    
    patient_sample_counts = lambda dirs: {d: len(os.listdir(d))/2 for d in dirs}
    
    # Count samples per patient to determine split amounts (after excluding patients to be used for eval)
    patient_dirs, test_holdout_dirs = [], []
    N_holdout = 0
    for data_dir in data_dirs:
        patient_dirs = [os.path.join(data_dir,d) for d in os.listdir(data_dir)]

        if test_holdout is not None:
            test_holdout_dirs = [d for d in patient_dirs if test_holdout(d)]
            holdout_counts_dict = patient_sample_counts(test_holdout_dirs)
            N_holdout = sum([count for count in holdout_counts_dict.values()])
            patient_dirs = list(set(patient_dirs) - set(test_holdout_dirs))
            
        counts_dict = patient_sample_counts(patient_dirs)
        patient_dir_counts = [(d,c) for d,c in counts_dict.items()]
        
        if not randomize:
            patient_dir_counts = sorted(patient_dir_counts, key=lambda t: -t[1])

        N = sum([count for (d,count) in patient_dir_counts])
        disp_str = "{} Patients, {} Samples"
        print(disp_str.format(len(patient_dirs), N))
        N_total = N + N_holdout
        if N_holdout:
            n_patients = len(test_holdout_dirs) + len(patient_dirs)
            print("Including holdout: " + disp_str.format(n_patients, N_total))

        n_train = int(split[0] * N)
        n_val = int(split[1] * N)
        n_test = N - n_train - n_val

        train_dirs, val_dirs, test_dirs = [], [], []
        train_count, val_count, test_count = 0, 0, 0

        for (patient_dir, n_samples) in patient_dir_counts:
            if train_count < n_train:
                train_dirs.append(patient_dir)
                train_count += n_samples
            elif val_count < n_val:
                val_dirs.append(patient_dir)
                val_count += n_samples
            else:
                test_dirs.append(patient_dir)
                test_count += n_samples

    # add any holdout patients to test set
    if test_holdout_dirs:
        test_dirs += test_holdout_dirs
        test_count += N_holdout
    
    s = '{} patients for {} ({} samples, {:4.2f}% of {})'
    print(s.format(len(train_dirs), 'training', train_count, train_count*100./N, N)) 
    print(s.format(len(val_dirs), 'validation', val_count, val_count*100./N, N)) 
    print(s.format(len(test_dirs), 'testing', test_count, test_count*100./N_total, N_total)) 
    
    if counts:
        return train_dirs, val_dirs, test_dirs, counts_dict, holdout_counts_dict
    else:
        return train_dirs, val_dirs, test_dirs



