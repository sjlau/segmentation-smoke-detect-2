"""
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.
"""
import tensorflow as tf
from tensorflow.keras.utils import Sequence
from datetime import datetime
import numpy as np
import os

from .. import ModelMGPU
from . import plot_utils as ph
from .metric_utils import calc_precision, calc_recall, calc_dice


def sort_on_predictions(dice_scores, **kwargs):
    """ Sorts the dice scores, metadata, images, labels  and predictions
    in parallel based on the dice scores. -1 is best, 0 is worst.
    """
    keys = list(kwargs.keys())
    vals = [kwargs[k] for k in keys]
    return dict(zip(['dice_scores', *keys],
                 zip(*sorted(zip(dice_scores, *vals)))))


def evaluate_model(model, test_data, sort_results=False, verbose=True):
    
    if isinstance(test_data, Sequence):
        dim = test_data[0][0].shape[1:3]
        if isinstance(model, ModelMGPU):
            model = model.get_layer('model_1')
        start_time = datetime.now()   
        probs = model.predict_generator(test_data)    
        predict_time = datetime.now() - start_time                        
    else:
        start_time = datetime.now()   
        probs = model.predict(test_data['X'])
        predict_time = datetime.now() - start_time                
        dim = test_data['X'].shape[1:3]
        
    probs = np.array(probs).reshape(len(probs), *dim)
    predictions = np.where(probs > 0.5, 1, 0).reshape(len(probs), *dim)

    # Evaluate predictions
    results, avgs = evaluate_predictions(predictions, test_data, verbose)
    if sort_results:
        # Sort on increasing dice score 
        if isinstance(test_data, Sequence):
            err_str = "Prediction sorting not supported for generators yet"
            raise NotImplementedError(err_str)
        else:
            results = sort_on_predictions(results['dice_list'],
                                          precisions=results['precision_list'],
                                          recalls=results['recall_list'],
                                          filenames=test_data['filenames'],
                                          X=test_data['X'],
                                          Y=test_data['Y'],
                                          predictions=predictions,
                                          probabilities=probs)
    else:
        results['predictions'] = predictions 
        results['probs'] = probs
    results['predict_time'] = predict_time.total_seconds()
    results['dice'] = avgs['dice'] 
    results['precision'] = avgs['precision']
    results['recall'] = avgs['recall']
    
    if verbose:
        print('\tPredict time (s): {}'.format(results['predict_time']))
    
    return results


def evaluate_predictions(predictions, labels, verbose=True):
    """ Iterates through a set of predictions and ground truths, totals up
    true pos, false pos etc., and calculates precision, recall, f-1 
    and dice scores.
    """
    
    if isinstance(labels, Sequence):
        bs = labels.batch_size
        ground_truth = lambda i: labels[i//bs][1][i%bs]
    else:
        if isinstance(labels, dict):
            err_str = "{}.evaluate_predictions: Unsupported label format"
            assert 'Y' in labels, err_str.format(__file__)
            ground_truth = lambda i: labels['Y'][i]
        else:
            ground_truth = lambda i: labels[i]
    
    dice_scores, precs, recs = [], [], []
    true_pos, false_pos, true_neg, false_neg = [], [], [], []
    
    for i, pred in enumerate(predictions):     
        
        label = ground_truth(i)
        metrics, _ = ph.compare_mask_prediction(ground_truth(i), pred, False)
        
        dice_scores.append(metrics['dice'])
        precs.append(metrics['precision'])
        recs.append(metrics['recall'])        
        true_pos.append(metrics['tp'])
        false_pos.append(metrics['fp'])
        true_neg.append(metrics['tn'])
        false_neg.append(metrics['fn'])
        
    total_tp = float(sum(true_pos)) 
    total_fp = float(sum(false_pos))
    total_tn = float(sum(true_neg)) 
    total_fn = float(sum(false_neg)) 
    
    precision = calc_precision(total_tp, total_fp)
    recall = calc_recall(total_tp, total_fn)
    dice = calc_dice(total_tp, total_fp, total_fn)
    
    print("\tprecision:", precision)
    print("\trecall:", recall)
    print("\tdice:", dice)
    individual_scores = {'dice_list': dice_scores, 
                         'precision_list': precs, 
                         'recall_list': recs}
    averages = {'dice': dice, 
                'precision': precision, 
                'recall': recall}
    
    return individual_scores, averages
           
