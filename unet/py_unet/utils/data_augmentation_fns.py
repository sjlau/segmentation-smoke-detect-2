"""                                                         
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell 
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.    
"""
from skimage.transform import warp, AffineTransform
from scipy import ndimage
from math import sqrt
import numpy as np
import cv2

from functools import partial


def gaussian_noise_fn(sigma):
    def _add_noise(img):
        noise = np.random.normal(0, sigma, img.shape[0]*img.shape[1])
        noise = noise.reshape((img.shape[0],img.shape[1]))
        img += noise
        return img
    return _add_noise


def rotate_fn(angle):
    """ Returns a function that rotates a Numpy matrix """
    return lambda img: ndimage.rotate(img, angle=angle, reshape=False)


def shear_fn(strength):
    """ Returns a function that shears a Numpy matrix """
    def _transform(img):     
        translation = (0, 0) if strength < 0.5 else (img.shape[0]//4, img.shape[1]//4)
        tform = AffineTransform(shear=strength, translation=translation)
        return warp(img, inverse_map=tform.inverse, output_shape=(img.shape))
    
    return _transform


def affine_fn(shear_strength, translation, rotation, scale):
    """ Returns a function that applies an arbitrary affine transformation to
    a Numpy matrix """
    def _transform(img): 
        tform = AffineTransform(shear=strength, translation=translation, rotation=rotation, scale=scale)
        return warp(img, inverse_map=tform.inverse, output_shape=(img.shape))
    
    return _transform


def shift_fn(scalex, scaley):
    """ Returns a function that translates a Numpy matrix """
    
    def _shift(img):
        
        shape = img.shape
        img_aug = img[:]
        translation = (int(shape[0]*scalex), int(shape[1]*scaley))
        
        # shift rows
        n_zero_rows = abs(translation[0])
        if translation[0] > 0:
            img_aug = np.concatenate([np.zeros((n_zero_rows, shape[1])), 
                                      img_aug[:-n_zero_rows, :]], 
                                     axis=0)
        else:
            img_aug = np.concatenate([img_aug[n_zero_rows:, :],
                                      np.zeros((n_zero_rows, shape[1]))],
                                     axis=0)
        # shift columns
        n_zero_cols = abs(translation[1])
        if translation[1] > 0:
            img_aug = np.concatenate([np.zeros((shape[0], n_zero_cols)), 
                                      img_aug[:, :-n_zero_cols]],
                                     axis=1)
        else:
            img_aug = np.concatenate([img_aug[:, n_zero_cols:],
                                      np.zeros((shape[0], n_zero_cols))],
                                     axis=1) 
        return img_aug
    
    return _shift


def zoom_fn(factor):

    def _clipped_zoom(img):
        height, width = img.shape[:2] # It's also the final desired shape
        new_height, new_width = int(height * factor), int(width * factor)

        ### Crop only the part that will remain in the result (more efficient)
        # Centered bbox of the final desired size in resized (larger/smaller) image coordinates
        y1, x1 = max(0, new_height - height) // 2, max(0, new_width - width) // 2
        y2, x2 = y1 + height, x1 + width
        bbox = np.array([y1,x1,y2,x2])
        # Map back to original image coordinates
        bbox = (bbox / factor).astype(np.int)
        y1, x1, y2, x2 = bbox
        cropped_img = img[y1:y2, x1:x2]

        # Handle padding when downscaling
        resize_height, resize_width = min(new_height, height), min(new_width, width)
        pad_height1, pad_width1 = (height - resize_height) // 2, (width - resize_width) //2
        pad_height2, pad_width2 = (height - resize_height) - pad_height1, (width - resize_width) - pad_width1
        pad_spec = [(pad_height1, pad_height2), (pad_width1, pad_width2)] + [(0,0)] * (img.ndim - 2)

        result = cv2.resize(cropped_img, (resize_width, resize_height), interpolation=cv2.INTER_AREA)
        result = np.pad(result, pad_spec, mode='constant')
        assert result.shape[0] == height and result.shape[1] == width
        return result
    
    return _clipped_zoom


"""
def zoom_fn(factor):

    def _clipped_zoom(img):

        h, w = img.shape[:2]
        zoom_tup = (factor, factor)
        
        if factor < 1:
            zh = int(h * factor)
            zw = int(w * factor)
            top = (h - zh) // 2
            left = (w - zw) // 2
            out = np.zeros_like(img)
            out[top:top+zh, left:left+zw] = ndimage.zoom(img, zoom_tup)

        elif factor > 1:
            zh = int(h / factor)
            zw = int(w / factor)
            top = (h - zh) // 2
            left = (w - zw) // 2
            out = ndimage.zoom(img[top:top+zh, left:left+zw], zoom_tup)
            trim_top = ((out.shape[0] - h) // 2)
            trim_left = ((out.shape[1] - w) // 2)
            out = out[trim_top:trim_top+h, trim_left:trim_left+w]
        else:
            out = img
        return out      
    
    return _clipped_zoom
"""
