"""                                                         
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell 
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.    
"""
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from copy import copy, deepcopy
from collections import defaultdict

from .metric_utils import calc_dice, calc_precision, calc_recall

DEFAULT_POS = 1.


def plot_train_histories(history, metrics, exp_num=None, save_path=None, 
                         loss_ylim=[0., 0.5],  metric_ylim=[0.5, 1.]): 
    """ """ 

    col = 1
    f, ax = plt.subplots(figsize=(15,5))
    for metric in metrics:
        sp = int('1' + str(len(metrics)) + str(col))
        plt.subplot(sp)  
        
        plt.title(metric)
            
        plt.plot(history.history[metric])  
        plt.plot(history.history['val_' + metric])  
        plt.ylabel(metric)  
        plt.xlabel('Epoch') 
        if 'loss' in metric:
            plt.ylim(loss_ylim)
        else:  
            plt.ylim(metric_ylim)
            
        plt.legend(['train', 'val'], loc='lower right')  
        col += 1
            
    plt.show()  
    if isinstance(save_path, str):
        plt.savefig(save_path, dpi=1000, facecolor='w', edgecolor='w', format='png',
                    transparent=False, bbox_inches=None, pad_inches=0.1)


def plot_dice(test_dice, train_dice=None, bins=25, save_path=None, figsize=(8,5)):
    
    # shut up stats.py
    import warnings; warnings.simplefilter('ignore')
    
    plt.figure(figsize=figsize)
    
    title = 'Test Set Dice Score Frequency Distribution'
    if train_dice is not None: 
        title = 'Train and ' + title
        
    plt.title(title, fontsize=18) 
    
    if np.max(test_dice) < 1.01:
        plt.xlim(0, 1)
    else:
        plt.xlim(0, 100)
    
    plt.xlabel('Dice Scores ({} bins)'.format(bins), fontsize=14)
    
    ax = sns.distplot(test_dice, bins=bins, color='blue', label='test')
    if train_dice is not None:
        ax = sns.distplot(train_dice, bins=bins, color='red', label='train')

    if isinstance(save_path, str):
        plt.savefig(save_path, dpi=1000, facecolor='w', edgecolor='w', format='png',
                    transparent=False, bbox_inches=None, pad_inches=0.1)
    plt.show()
        
        
def display_pred_comparison(truth, pred, img, title=None, pos_class=1, save=False, fname=None):
    
    metrics, truth_vs_pred_img = compare_mask_prediction(truth, pred, vis=False, pos=pos_class)

    overlaid_contour_img = overlay_contour(img, truth, vis=False, pos=pos_class)
    overlaid_pred_img = overlay_contour(img, pred, vis=False, pos=pos_class)
    
    fig, axs = plt.subplots(1,3,figsize=(20,7))

    plt.tight_layout(True)
    if title:
        plt.subplots_adjust(top=0.85)        
        fig.suptitle(title, fontsize=23)

    axs[0].imshow(truth_vs_pred_img)
    axs[0].set_title('Comparison', fontsize=20)
    axs[0].set_xticks([])
    axs[0].set_yticks([])
    
    axs[1].imshow(overlaid_pred_img, cmap = 'gray')
    axs[1].set_title('Prediction', fontsize=20)
    axs[1].set_xticks([])
    axs[1].set_yticks([])
    
    axs[2].imshow(overlaid_contour_img, cmap = 'gray')
    axs[2].set_title('Ground Truth', fontsize=20)
    axs[2].set_xticks([])
    axs[2].set_yticks([])  
    
    if save:
        fname = fname if fname else 'comparison.png'
        fig.savefig(fname, dpi=500, facecolor='w', edgecolor='w', format='png',
                    transparent=False, bbox_inches=None, pad_inches=0.1)
        
    return truth_vs_pred_img, overlaid_pred_img, overlaid_contour_img
        
        
def overlay_contour(image, contour, pos=DEFAULT_POS, logits=False, 
                    vis=True, size=(6,6)):
    """ Overlays a contour onto an image.
    Parameters:
        image   -- The frame corresponding to the contour
        contour -- The contour to overlay on the image
        pos     -- Value representing a positive sample, i.e., the value
                   that pixels representing the contour take on (1 or 0)
        vis     -- Set to false to return the overlaid image instead of displaying
        size    -- Size of the matplotlib figure to display

    Returns:
        Nothing if vis is True, the overlaid image if vis is False.
    """
    
    image = image[:,:,0] if len(image.shape) > 2 else image
    contour = contour[:,:,0] if len(contour.shape) > 2 else contour
    
    dcm = deepcopy(image)
    dcm = np.stack((dcm,)*3, -1)
    
    if logits:
        green_multiplier = logits * [0, 1, 0]
    else:
        green_multiplier = [0, 1, 0]
    mask = deepcopy(contour)
    mask = np.array([[True if c == pos else False for c in row] for row in mask])
    dcm[mask, :] *= green_multiplier

    if vis:
        # if vis is true, display here
        plt.figure(figsize=size)
        plt.imshow(dcm)
    else:
        # If not, return the overlaid image
        return dcm
    

    
def compare_mask_prediction(truth, pred, vis=True, pos=DEFAULT_POS, **kwargs):
    """ Function to calculate the pixel-wise true positives, true negatives, 
    false positives, and false negatives in one prediction. If vis=True,
    it will display an image where... 
    Green: true positives
    Red:   false positives
    Blue:  false negatives
    White: true negatives
    """

    pos, neg = (0.,1.) if pos == 0. else (1.,0.)
    if len(truth.shape) > 2: truth = truth[:,:,0]
    if len(pred.shape) > 2: pred = pred[:,:,0]
    
    tp, fp, tn, fn = 0., 0., 0., 0.  
    img = np.zeros((truth.shape[0],truth.shape[1],3))
    
    for i, (row_truth, row_pred) in enumerate(zip(truth, pred)):
        row_truth, row_pred = row_truth.tolist(), row_pred.tolist() 

        row = np.zeros((truth.shape[0],3))
        for j, (pixel_truth, pixel_pred) in enumerate(zip(row_truth, row_pred)):
            
            if pixel_pred not in [0., 1.]: pixel_pred = np.round(pixel_pred)
            if pixel_truth not in [0., 1.]: pixel_truth = np.round(pixel_truth)
            
            if neg == pixel_truth == pixel_pred:
                tn += 1.
                row[j, :] = [1., 1., 1.]
            elif neg == pixel_truth and pos == pixel_pred:
                fp +=1.
                row[j, :] = [1., 0., 0.]
            elif pos == pixel_truth and neg == pixel_pred:
                fn += 1.
                row[j, :] = [0., 0., 1.]
            elif pos == pixel_truth == pixel_pred:
                tp += 1.
                row[j, :] = [0., 1., 0.]
            else:
                print(pixel_truth, pixel_pred)
        img[i] = row

    dice = calc_dice(tp, fp, fn)
    prec = calc_precision(tp, fp)
    rec = calc_recall(tp, fn)
    
    if vis:
        display_str = "true pos: {}; false pos: {}; true neg: {}; false neg: {}"
        print(display_str.format(tp, fp, tn, fn))
        print("dice coeff: {}".format(dice))
        print("precision: {}".format(prec))
        print("recall: {}".format(rec))

       
        plt.figure(figsize=(6,6))
        plt.imshow(img)
    
    metrics = {
        'dice': dice, 'precision': prec, 'recall': rec,
        'tp': tp, 'fp': fp, 'tn': tn, 'fn': fn
    }
    return metrics, img

