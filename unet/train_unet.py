import tensorflow as tf
from tf.keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from tf.keras.optimizers import Adam
from tf.keras import backend as K
from matplotlib import pyplot as plt
from datetime import datetime
import numpy as np
import sys
import os

from py_unet.utils import plot_utils 
from py_unet.utils.metric_utils import bce_dice_loss, dice_coef, dice_loss
from py_unet.data_generator import DataGenerator
from py_unet.unet_builder import UnetBuilder
from py_unet.utils.results_logger import ResultsLogger

K.set_image_dim_ordering('tf')  
K.set_image_data_format('channels_last')  # Data format: (n_examples, img_dim, img_dim, n_channels)



def _log(logger, key, val):
    is_empty = lambda v: (v is None or (isinstance(v, list) and len(v) == 0))
    if logger is not None:
        if is_empty(val):
            val = 'None'
        logger.add(key, val)
    

def import_config(cfg_fname=None):
    """ Imports args, either from a file specified in argv
    or from a file named like this one, but ending in _cfg.py
    """
    if cfg_fname:
        myconfig = cfg_fname
    elif len(sys.argv) > 1:
        myconfig = sys.argv[1]
    else:
        myconfig = __file__.replace('.py', '_cfg')

    if myconfig.endswith('.py'):
        myconfig = myconfig.replace('.py','')

    cfg = __import__(myconfig)    
    return cfg
        
        
def create_data_generators(data_args, logger=None):
    """ Creates training, validation and test data generators based on values found in 
    the data Namespace of the config file
    """

    data_root = data_args.root_dir
    data_dirs = [os.path.join(data_root, d) for d in data_args.data_dirs]
    _log(logger, 'data_dirs', data_dirs)    

    print("\nLoading Data Directories...")    
    train_dirs, val_dirs, test_dirs = get_split_dirs(data_dirs,
                                                     data_args.create_patient_split, 
                                                     data_args.split, 
                                                     data_args.randomize_split,
                                                     test_holdout=data_args.test_holdout) 

    batch_size = data_args.batch_size  
    dim = (data_args.image_size, data_args.image_size)  
    use_aug = not data_args.augmentation.disable
    aug_params = data_args.augmentation.param_dict if use_aug else None
    
    _log(logger, 'batch_size', batch_size)    
    _log(logger, 'augmentations', aug_params)        
    
    print("\nSetting up Data Generators...")       
    train_gen = DataGenerator(train_dirs, batch_size, dim,
                              use_augmentation=use_aug, 
                              aug_fn_args=aug_params,
                              n_aug_copies=len(aug_params))

    val_gen = DataGenerator(val_dirs, batch_size, dim)
    test_gen = DataGenerator(test_dirs, batch_size, dim)
    
    return train_gen, val_gen, test_gen


def build_unet(hp, n_gpus, logger=None):
    """ Builds and compiles a U-Net, optionally loading a trained state and freezing layers
    """

    print("\nBuilding a U-Net...")     
    model_builder = UnetBuilder(hp.input_dim, 
                                hp.unet_layer_filters, 
                                dropout=hp.dropout, 
                                batch_norm=hp.batch_norm,
                                kernel_initializer=hp.kernel_initializer,
                                kernel_init_seed=int(hp.kernel_init_seed),
                                kernel_regularizer=hp.kernel_regularizer,
                                weight_decay=hp.weight_decay,
                                n_gpus=n_gpus)

    model = model_builder.build()
    model.compile(optimizer=hp.opt(lr=hp.lr), 
                  loss=hp.loss,  
                  metrics=hp.metrics)

    if hp.load_trained_model:
        print("\nLoading trained model ({})...".format(hp.load_trained_model))
        model = utils.load_model_state(model, hp.load_trained_model, hp.models_dir)
        
    if hp.freeze_unet_blocks:
        print("\nFreezing blocks ({})...".format(hp.freeze_unet_blocks))                                     
        model = utils.freeze_unet_blocks(model, hp.freeze_unet_blocks)
        _log(logger, 'frozen_blocks', hp.freeze_unet_blocks)  
              
        print("\nRecompiling...")
        model.compile(optimizer=hp.opt(lr=hp.lr), 
                      loss=hp.loss,  
                      metrics=hp.metrics)
        

    _log(logger, 'lr', hp.lr)    
    _log(logger, 'dropout', hp.dropout)
    _log(logger, 'batch_norm', hp.batch_norm)
    _log(logger, 'kernel_init', hp.kernel_initializer)
    _log(logger, 'kernel_init_seed', hp.kernel_init_seed)
    _log(logger, 'kernel_regularlizer', hp.kernel_regularizer)
    _log(logger, 'weight_decay', hp.weight_decay)
    _log(logger, 'loaded_model_name', hp.load_trained_model)  

    return model


def main(args):
 
    results_dir = os.path.join(args.OUTPUT_DIR, 'results')   
    
    # initialize logger if desired by caller
    if args.LOGGING:
        logname = args.JOB_NAME
        if '-run' in logname:
            logname = logname.split('-run')[0]
        print("Logging results at {}/{}".format(results_dir, logname))
        logger = ResultsLogger(name=logname, out_dir=results_dir)
        logger.load()
        logger.show()    
    else:
        logger = None
    
    # prepare data generators
    train_gen, val_gen, test_gen = create_data_generators(args.data, logger)

    # configure compute devices 
    n_gpus = utils.device_config()
    _log(logger, 'gpus', n_gpus)

    # build a model
    hp = args.training
    model = build_unet(hp, n_gpus, logger)
    
    # train
    print("\nTraining...")
    start_time = datetime.now()
    train_hist = model.fit(x=train_gen,
                           epochs=hp.epochs,
                           validation_data=val_gen,
                           callbacks=hp.callbacks,
                           verbose=0)
    train_time = datetime.now() - start_time

    _log(logger, 'train_time', train_time.total_seconds())  
    _log(logger, 'n_epochs', len(train_hist.history['loss']))
    
    ph.plot_train_histories(train_hist, ['dice_coef', 'loss'])    
    plt.savefig(os.path.join(results_dir, args.JOB_NAME + '_loss.png'))    

    # Get predictions and evaluative metrics
    results = utils.evaluate_model(model, test_gen.load_all())

    # Log results
    _log(logger, 'predict_time', results['predict_time'])    
    _log(logger, 'dice', results['dice'])
    _log(logger, 'precision', results['precision'])
    _log(logger, 'recall', results['recall'])
    
    # Save model and log to h5 files
    utils.save_model_state(model, args.JOB_NAME, hp.models_dir)  
    logger.show()
    logger.write()
    
    
if __name__ == '__main__':
    print(sys.argv)
    main(import_config())
    
