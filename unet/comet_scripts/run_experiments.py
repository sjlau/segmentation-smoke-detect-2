import sys
sys.path.append('..')
import train_unet


if __name__ == '__main__':

    n_runs = 1
    if len(sys.argv) > 2:
        n_runs = int(sys.argv[2])
    if len(sys.argv) > 1:
        cfg_name = sys.argv[1]
    else:
        print("usage: python3 run_experiments.py config_filename [n_runs=1]")
        sys.exit(-1)      

    args = train_unet.import_config(cfg_name)
    NAME = args.JOB_NAME

    args.data.augmentation.disable = True
    for run in range(n_runs):
        run_name = '{}-run{}'.format(NAME, run)
        log = 'logs/{}'.format(run_name)
        args.JOB_NAME = run_name
        with open(log, 'a') as f:
            sys.stdout = f
            train_unet.main(args)
        

    args.data.augmentation.disable = False
    for run in range(n_runs):
        run_name = '{}-AUG-run{}'.format(NAME, run)
        log = 'logs/{}'.format(run_name)
        args.JOB_NAME = run_name
        with open(log, 'a') as f:
            sys.stdout = f
            train_unet.main(args)
            

        
