import sys
sys.path.append('../py_unet')
from py_unet.utils.metric_utils import bce_dice_loss, dice_coef
from py_unet.utils.model_utils import step_decay_fn

# Keras etc.
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, LearningRateScheduler
from keras.initializers import Orthogonal, glorot_normal, he_normal
from keras.regularizers import l2, l1
from keras.optimizers import Adam
from datetime import datetime
from argparse import Namespace
import os

JOB_NAME = '2a'  #'JOB_TRAIN_{}'.format(datetime.now().isoformat().replace(':', '.'))
LOGGING = True

OUTPUT_DIR = '/oasis/projects/nsf/csd538/mhnguyen/project-share/ucsd-medical/code/git-repos/ucsd-medical/unet'
os.chdir(OUTPUT_DIR)

data = Namespace(
    root_dir = '/oasis/projects/nsf/csd538/mhnguyen/project-share/ucsd-medical/data/preprocessed',
    data_dirs = ['acdc_176_npy', 'sb_176_npy'],

    batch_size = 4,    
    split = [0.74, 0.15, 11.],
    sample = None,
    test_holdout = None,

    create_patient_split = True,
    randomize_split = False,
    image_size = 176,

    augmentation = Namespace(
        disable = False,
        additive = False,        
        param_dict = {
          'horizontal_flip': 0.5,
          'vertical_flip': 0.5,
          'shear': (0.5, (-.25, .25)),
          'shift': (0.5, (-0.05, 0.05),(-0.05, 0.05)),
          'zoom': (0.5, (0.8, 1.05)),
          'add_gaussian_noise': (0.5, (0.01, 0.09))
        }
    )
)

training = Namespace(
    models_dir = os.path.join(OUTPUT_DIR, 'trained_models'),
    input_dim = (data.image_size,)*2 + (1,),
    
    unet_layer_filters = [8, 16, 32, 64, 128],
    dropout = True,
    batch_norm = False,
    
    load_trained_model= None,
    freeze_unet_blocks = [],

    opt =  Adam,
    lr = 0.0005,
    epochs = 200,
    loss =  bce_dice_loss,
    metrics =  [dice_coef, bce_dice_loss] ,
    
    
    callbacks = [

        EarlyStopping(monitor='val_bce_dice_loss', 
                      mode='min',
                      patience=15, 
                      restore_best_weights=True),

        ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=10,
                          verbose=1, mode='auto', min_delta=0.0001,
                          cooldown=0, min_lr=0)  

    ],
    
    kernel_initializer = Orthogonal,
    kernel_init_seed = 42.,
    kernel_regularizer = l2,
    weight_decay = 1e-4,
    
)

