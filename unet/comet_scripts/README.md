## Contents

run_experiments.slrm
* Main script that dictates what will be executed by the SLRM job
* Here, we execute a `run_experiments.py` within a Singularity container that<br>
holds all necessary Python dependencies 

run_experiments.py
* Python script to run multiple iterations of a given experiment, potentially<br>
with different parameter configurations
* Here, we run `train_unet.py` multiple times, with and without data<br>
augmentation enabled

cfg_<experiment>.py
* Configuration file for `train_unet.py`, with variables grouped into namespaces<br>
for convenience of reference (see `train_unet.py`)

## Running Jobs on Comet
Notes on Comet's resources available [here](https://docs.google.com/document/d/1vGqVbEVhCsJevrsjtAWnhJkt3lNx8WIpYpHxTzGRHK8/edit)

### Interactive Jobs
Running interative jobs is one way to use Comet's compute resources. Examples<br>
given below:

* Using compute nodes (if you're not training anything/don't need GPUs)<br>
`srun --partition=compute --pty --nodes=1 --ntasks-per-node=24 -t 08:00:00 --wait=0 --export=ALL /bin/bash`

* Using GPU-Shared with 1 K80 GPU for 10 hours<br>
`srun --partition=gpu-shared --gres=gpu:k80:1 --pty --nodes=1 --ntasks-per-node=6 -t 10:00:00 --wait=0 --export=ALL /bin/bash`