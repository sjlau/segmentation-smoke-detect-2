import numpy as np
import dicom
import cv2
import os
import re
import shutil
import fnmatch
from collections import defaultdict

from preprocessor_base import Preprocessor
import util


def shrink_case(case):
    toks = case.split('-')
    def shrink_if_number(x):
        try:
            cvt = int(x)
            return str(cvt)
        except ValueError:
            return x
    return '-'.join([shrink_if_number(t) for t in toks])


class Contour(object):
    def __init__(self, ctr_path):
        self.ctr_path = ctr_path
        match = re.search(r'/([^/]*)/contours-manual/IRCCI-expert/IM-0001-(\d{4})-.*', ctr_path)
        self.case = shrink_case(match.group(1))
        self.img_no = int(match.group(2))
    
    def __str__(self):
        return '<Contour for case %s, image %d>' % (self.case, self.img_no)
    
    __repr__ = __str__


class SunnybrookPreprocessor(Preprocessor):
    
    
    def __init__(self, crop_size, invert_labels=False, verbose=False,
                 img_ctr_map_fpath='./SAX_series.txt', contour_type='i'):
        """
        The purpose of this class is to load images (from dicom), preprocess them
        and write the resulting images to hdf5 files. 
        
        args:
            hdf5_out_path -- directory to write hdf5 files to 
            crop_size -- one integer, applies to both heigh and width of square cropping
            invert_labels -- True if binary labels should be inverted
            verbose -- True for status print messages
            
        For reference:
        
            0: SC-N (healthy)
            1: SC-HF-I (heart failure with infarction)
            2: SC-HF-NI (heart failure with no infarction)
            3: SC-HYP (LV hypertrophy)

            counts: {0: 142, 1: 245, 2: 234, 3: 184}    
        
        """     
        self.crop_size = crop_size
        self.invert_labels = invert_labels
        self.verbose = verbose
        
        self.data = {}
        #self.img_counts = {'ED': 0, 'ES': 0}
        self.img_count = 0
        self.errors = []
        self.processed = []      
    
        # sunnybrook specific stuff
        self.img_ctr_map_fpath = img_ctr_map_fpath
        self.contour_type = contour_type
        
        self.pathology_dict = {'N': 0,'HFI': 1,'HFNI': 2,'HYP': 3}
        self.pathology_counts = {0: 142, 1: 245, 2: 234, 3: 184} # hacky
        
        self.sax_series = {}
        with open(img_ctr_map_fpath, 'r') as f:
            for line in f:
                if not line.startswith('#'):
                    key, val = line.split(':')
                    self.sax_series[key.strip()] = val.strip()   

   
    def load(self, data_dirs):   
        """ 
        args:
            data_dirs -- dictionary of directories pertaining to the original sunnybrook split
                         first keys should be "train", "val", "test". Each of these points to 
                         a dict where keys are "images" and "labels", pointing to the corresponding
                         directories.         
        """
              
        self.data = defaultdict(list)

        num_pathologies = len(self.pathology_dict)        
        get_pathology = lambda dcm: self.pathology_dict[self._pathology_name(dcm)]
                
        
        for dataset in data_dirs:
            
            contours = self._get_contour_objects(data_dirs[dataset]['labels'])          
            images, labels, meta, _ = self._get_images_and_contours(contours, data_dirs[dataset]['images'])
       
            # organize data by patient 
            for img, mask, dcm in zip(images, labels, meta):
                img, mask = self._preprocess(img, dcm, mask)
                sample = {'image': img,'label': mask, 'dcm': dcm}
                self.data[self._patient_name(dcm)].append(sample) 
                self.img_count += 1

        
    def _preprocess(self, img, dcm, mask=None):
        """ Preprocess an image, potentially with its corresponding mask """
        
        img = cv2.resize(img.astype(np.float64), (0, 0), fx=dcm.PixelSpacing[0], fy=dcm.PixelSpacing[1])
        mask = cv2.resize(mask, (0, 0), fx=dcm.PixelSpacing[0], fy=dcm.PixelSpacing[1])

        if img.ndim < 3: img = img[..., np.newaxis].astype(np.float16)
        if mask is not None and mask.ndim < 3:  mask = mask[..., np.newaxis]

        img = util.center_crop(img, self.crop_size)
        mask = util.center_crop(mask, self.crop_size)

        clahe = cv2.createCLAHE(tileGridSize=(4,4), clipLimit=2.)
        img = clahe.apply(np.array(img[:,:,0], dtype=np.uint16))[..., np.newaxis]

        img = util.scale(img)
        if mask is not None and mask.ndim < 3:  mask = mask[..., np.newaxis]

        return img, mask

    
    def write(self, out_path, mode='split', ftype='numpy', split=[0.75, 0.15, .10], overwrite=False):
                   
        if mode == 'patients':
            pass
            #self._write_patient_dirs(out_path, ftype, overwrite)
        elif mode == 'split':
            self._write_split_dirs(out_path, ftype, split, overwrite)
      
    
    def _write_split_dirs(self, out_path, ftype='numpy', split=[0.75, .15, .10], overwrite=False):
        
        try:
            self._prep_split_dirs(out_path, overwrite)
        except Exception as e:
            print('_write_split_dirs: exiting because of exception:\n{}'.format(e))
            return
                  
        counts = {'train': 0, 'val': 0, 'test': 0}
        n_train = int(split[0] * self.img_count)
        n_val = int(split[1] * self.img_count)
        
        for patient in self.data:
            
            if counts['train'] < n_train:
                split_dir = 'train'
            elif counts['val'] < n_val:
                split_dir = 'val'
            else:
                split_dir = 'test'
                
            patient_out_dir = os.path.join(out_path, split_dir)

            for idx in range(len(self.data[patient])):
                img = np.array(self.data[patient][idx]['image'])
                label = np.array(self.data[patient][idx]['label'])
                
                if ftype == 'numpy':

                    img_fname = '{}_{}_image.npy'.format(patient, idx)
                    label_fname = '{}_{}_mask.npy'.format(patient, idx)
                    print("{}\t{}".format(img_fname, label_fname))

                    np.save(os.path.join(patient_out_dir, img_fname), img)                    
                    np.save(os.path.join(patient_out_dir, label_fname), label)
                    
                    counts[split_dir] += 1
                    
                    

    
    def _get_images_and_contours(self, contours, data_path):   
        print('\tProcessing {:d} images and labels ...\n'.format(len(contours)))

        images = np.zeros((len(contours), 256, 256))
        masks = np.zeros((len(contours), 256, 256))
        filenames = []
        metadata = []
        for idx, contour in enumerate(contours):
            #img, mask, dcm, img_path, mask_path = self._get_img_and_contour(contour, data_path)
            filename = 'IM-%s-%04d.dcm' % (self.sax_series[contour.case], contour.img_no)
            full_path = os.path.join(data_path, contour.case, filename)
            
            dcm = dicom.read_file(full_path)
            img = dcm.pixel_array.astype('int')
            
            mask = np.zeros_like(img, dtype='uint8')
            coords = np.loadtxt(contour.ctr_path, delimiter=' ').astype('int')
            cv2.fillPoly(mask, [coords], 1)
            
            images[idx] = img
            masks[idx] = mask
            metadata.append(dcm)
            
            filenames.append({'image': full_path, 'contour': contour.ctr_path})

        return images, masks, metadata, filenames    
    
    
    def _get_contour_objects(self, contour_path):
        self._print('\tMapping ground truth '+self.contour_type+'-contours to images...')
        contours = [os.path.join(dirpath, f) 
                    for dirpath, dirnames, files in os.walk(contour_path)
                    for f in fnmatch.filter(files, 'IM-0001-*-'+self.contour_type+'contour-manual.txt')]
        
        self._print('\tNumber of examples: {:d}'.format(len(contours)))
        contours = list(map(Contour, contours))
        return contours
    
    
    def _pathology_name(self, dcm):        
        p = str(dcm.PatientsName).split('-') 
        return p[1] + p[2] if p[1] == 'HF' else p[1]

    
    def _patient_name(self, dcm):        
        p = str(dcm.PatientsName).split('-') 
        return p[1] + p[2] + p[3] if p[1] == 'HF' else p[1] + p[2]
    
    
    def show_sample_img(self, patient=None, i=None):
        p = patient if patient else np.random.choice(list(self.data.keys()))
        i = i if i else np.random.randint(0, len(self.data[p]))
        img = self.data[p][i]['image'][:,:,0]
        ctr = self.data[p][i]['label']
        util.overlay_contour(img, ctr, pos=1, vis=True)
    
    
    def _print(self, *s):
        if self.verbose:
            for c in s:
                print(c, end='')
            print()
            
            
if __name__ == '__main__':
    pass