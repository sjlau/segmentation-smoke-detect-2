from abc import ABC, abstractmethod
import os
import shutil

class Preprocessor(ABC):
    
    @abstractmethod
    def load():
        """ Loads from native format of dataset, either DICOM or NIfTI """
        return NotImplemented
    
    @abstractmethod
    def _preprocess():
        return NotImplemented 
    
    @abstractmethod    
    def write():
        return NotImplemented         
           
    def _prep_split_dirs(self, out_dir, overwrite):
 
        for t in ['train', 'val', 'test']:
            path = os.path.join(out_dir, t)
            if os.path.exists(path):
                if overwrite:
                    shutil.rmtree(path)
                else:
                    e = 'Directory {} already exists; overwrite set to False'
                    raise Exception(e.format(path)) 

            os.makedirs(path)
