import numpy as np
import dicom
import h5py
import cv2
import os
import shutil

from preprocessor_base import Preprocessor
import acdc_helpers
import util


class ACDCPreprocessor(Preprocessor):
    
    
    def __init__(self, crop_size, target_label=3, invert_labels=False, verbose=False):
        """
        The purpose of this class is to load images (from dicom), preprocess them
        and write the resulting images to hdf5 files. 
        
        args:
            crop_size -- one integer, applies to both heigh and width of square cropping
            invert_labels -- True if binary labels should be inverted
            verbose -- True for status print messages
        
        """
        
        self.crop_size = crop_size
        self.target_label = target_label
        self.invert_labels = invert_labels
        self.verbose = verbose
        
        self.data = {}
        #self.errors = []
        #self.processed = []      
        self.n_discarded = 0
        
        
    def load(self, data_dirs):
        
        # Iterate over train, val, test
        for name in data_dirs:
            images, masks, fpaths = self._load_set(data_dirs[name])
            self.data[name] = {'images': images,
                               'labels': masks,
                               'filepaths': fpaths}
          
        
   
    def _load_set(self, dir_path, target_label=3):

        all_files = sorted(os.listdir(dir_path))

        images, labels, files = [], [], []
        for fname in all_files:        

            f = h5py.File(os.path.join(dir_path, fname), 'r')        
            image, label = np.array(f['image']), np.array(f['label'])

            ps = f['pixel_spacing']
            roi_c = f['roi_center']
            roi_r = f['roi_radii']

            image, label = self._preprocess(image, label, ps, roi_c, roi_r)
            if image is not None and label is not None:
                images.append(image)
                labels.append(label)
                files.append(fname)

            else:            
                # discard, no target label found
                self.n_discarded += 1

        return images, labels, files
    
            
    def _preprocess(self, img, mask, pixel_spacing, roi_center=None, roi_radii=None):
        """ Preprocess an image, potentially with its corresponding mask """

        # Scale image dimensions and ROI measurements by pixel spacing
        img = cv2.resize(img, (0, 0), fx=pixel_spacing[0], fy=pixel_spacing[1])
        mask = cv2.resize(mask, (0, 0), fx=pixel_spacing[0], fy=pixel_spacing[1])
        roi_center = [roi_center[0]*pixel_spacing[0], roi_center[1]*pixel_spacing[1]]
        roi_radii = [roi_radii[0]*pixel_spacing[0], roi_radii[1]*pixel_spacing[1]]

        if roi_center and roi_radii:
            img, mask = acdc_helpers.roi_crop(img, mask, roi_center, roi_radii)

        if self.target_label:
            filt = lambda p: 1 if p == self.target_label else 0
            if mask.ndim > 2: mask = mask[:,:,None]
            mask = np.array([[filt(px) for px in row] for row in mask])
            if sum(sum(mask)) == 0:
                return None, None
            mask = mask = mask[..., np.newaxis]

        clahe = cv2.createCLAHE(tileGridSize=(4,4), clipLimit=3.)
        img = clahe.apply(np.array(img[:,:,0], dtype=np.uint16))[..., np.newaxis]
        
        img = util.scale(img)
        
        if img.ndim < 3: img = img[..., np.newaxis].astype(np.float16)
        if mask.ndim < 3: mask = mask[..., np.newaxis]
            
        return img, mask


    
    def write(self, out_path, mode='split', ftype='numpy', overwrite=False):
        
        if mode == 'patients':
            pass # todo
       
        elif mode == 'split':
            self._write_split_dirs(out_path, ftype, overwrite)
 
                    
                    
    def _write_split_dirs(self, out_path, ftype='numpy', overwrite=False):
        
        try:
            self._prep_split_dirs(out_path, overwrite)
        except Exception as e:
            print('_write_split_dirs: exiting because of exception:\n{}'.format(e))
            return
        
        
        for name, dataset in self.data.items():
            self._print(name)

            images = dataset['images']
            labels = dataset['labels']
            fpaths = dataset['filepaths']
         
            split_dir = os.path.join(out_path, name)
                    
            for idx in range(len(images)):
                img = np.array(images[idx])
                label = np.array(labels[idx])
                fpath = 'acdc_' + ''.join(fpaths[idx].split('.')[:-1]) + '_{}.npy'
       
                img_fname = fpath.format('image')
                label_fname = fpath.format('mask')
                self._print("{}\t{}".format(img_fname, label_fname))

                np.save(os.path.join(split_dir, img_fname), img)                    
                np.save(os.path.join(split_dir, label_fname), label)
                            
                    
    def show_sample_img(self):
        p = np.random.choice(list(self.data.keys()))
        i = np.random.randint(0, len(self.data[p]))
        img = self.data[p]['images'][i][:,:,0]
        ctr = self.data[p]['labels'][i][:,:,0]
        util.overlay_contour(img, ctr, pos=1, vis=True)
        
        
    def _print(self, *s):
        if self.verbose:
            for c in s:
                print(c, end='')
            print()
            
            
if __name__ == '__main__':
    pass