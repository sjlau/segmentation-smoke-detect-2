import numpy as np
# import dicom
import h5py
import cv2
import os
import shutil

from preprocessor_base import Preprocessor
import util


class MicePreprocessor(Preprocessor):
    
    
    def __init__(self, crop_size, context_type=None, invert_labels=False, verbose=False):
        """
        The purpose of this class is to load images (from dicom), preprocess them
        and write the resulting images to hdf5 files. 
        
        args:
            data_dirs -- list of directories that contain patient directories (see self.load())
            hdf5_out_path -- directory to write hdf5 files to (see self.write_hdf5())
            crop_size -- one integer, applies to both heigh and width of square cropping
            invert_labels -- True if binary labels should be inverted
            verbose -- True for status print messages
        
        """
        
        self.crop_size = crop_size
        self.context_type = context_type
        self.invert_labels = invert_labels
        self.verbose = verbose
        
        self.data = {}
        self.img_counts = {'ED': 0, 'ES': 0}
        self.errors = []
        self.processed = []      
        
        self._reshape_mask = lambda m, dcm: m.reshape(dcm.Columns, dcm.Rows).T

   
    def load(self, data_dirs):   
        """ 
        Iterates through data_dirs, which are full paths to collections of patient 
        directories.
        It loads all the images for each patient along with the corresponding masks. 
        The masks come in a single matrix that must be chopped up according to the shape 
        of the corresponding images.
        The image, mask pairs are preprocessed (see self._preprocess) and stored in 
        self.data, a dict with structure like:
            self.data[patient_id][ED/ES][images/labels/dcm]
        
        Each data directory is expected to have the following structure:       
        - data_dir_A/
            - patient1/
                - ED/
                    - ED images and label file
                - ES/
                    - ES images and label file
            - patient2/
              ...
        - data_dir_B/
          ....
 
        """
            
        self._print("Loading images and labels...")
        for data_dir in data_dirs:

            patients = os.listdir(data_dir)
            
            for patient in patients:
                
                # Handle directory structures with and without SAX dir
                patient_dir = os.path.join(data_dir, patient)    
                if not os.path.exists(os.path.join(patient_dir, 'ED')):
                    patient_dir = os.path.join(data_dir, patient, 'SAX')

                self.data[patient] = {'ES': {'images': [],
                                             'labels': [],
                                             'dcm': []},
                                      'ED':  {'images': [], 
                                             'labels': [], 
                                             'dcm': []}}
                
                for sample_type in ['ES', 'ED']:

                    images_path = os.path.join(patient_dir, sample_type) 
                    if not os.path.exists(images_path):
                        self._print('{} does not exist. skipping.'.format(images_path))
                        continue
                    
                    patient_files = self._get_patient_files(images_path, sample_type)
                    if len(patient_files) == 0:
                        continue

                    image_files = patient_files[:-1] 
                    label_file = os.path.join(images_path, patient_files[-1])
                    
                    n_images = len(image_files)
                    self.processed.append((n_images, patient_dir))  

                    # Get all the masks for this set of images
                    masks = np.fromfile(label_file, dtype='uint8') 
                    masks = np.array(np.array_split(masks, n_images))

                    self._process_patient_samples(patient, images_path, image_files, 
                                                  masks, sample_type)


        self._print("... done.")
        self._print("ED count:", self.img_counts['ED'])
        self._print("ES count:", self.img_counts['ES'])

            
    def _preprocess(self, img, dcm, mask=None):
        """ Preprocess an image, potentially with its corresponding mask """
        
        img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
        mask = cv2.resize(mask, (0, 0), fx=0.5, fy=0.5)

        if img.ndim < 3: img = img[..., np.newaxis].astype(np.float16)
        if mask is not None and mask.ndim < 3:  mask = mask[..., np.newaxis]

        img = util.center_crop(img, self.crop_size)
        mask = util.center_crop(mask, self.crop_size)

        clahe = cv2.createCLAHE(tileGridSize=(1,1), clipLimit=1.)
        img = clahe.apply(np.array(img[:,:,0], dtype=np.uint16))[..., np.newaxis]

        img = util.scale(img)

        return img, mask

    
    def _get_patient_files(self, path, sample_type):
        """ Get all the files in a patient's directory """
        
        if len(os.listdir(path)) == 1:
            path = os.path.join(path, os.listdir(path)[0])
            
        patient_files = os.listdir(path)
        patient_files = util.natural_sort(patient_files)
        
        # get filenames for the labeled frames
        patient_files = [p for p in patient_files if '_' not in p]

        if sample_type + '.mat' in patient_files:
            patient_files.remove(sample_type + '.mat')
            
        return patient_files
    
    
    def _process_patient_samples(self, patient, images_dir, image_files, masks, sample_type):
        
         # Iterate over images and corresponding masks and preprocess 
        for i, (fpath, mask) in enumerate(zip(image_files, masks)):

            full_path = os.path.join(images_dir, fpath)                 
            dcm = dicom.read_file(full_path)

            if i == 0: 
                self._print('\n\tStudy ID: ', dcm.StudyID)
                self._print('\t', len(image_files), 'images:', image_files)
                self._print('\tImage Shape:', (dcm.Rows, dcm.Columns))
                self._print('\tMask Shape:', mask.shape)
            self._print(fpath, '...')

            if dcm.Rows * dcm.Columns != mask.shape[0]:
                err_str = 'Failed to load {}; had image shape {} with mask shape {}'
                err_str = err_str.format(dcm.StudyID, (dcm.Rows, dcm.Columns), mask.shape)
                self._print('ERROR: ' + err_str)
                self.errors.append(err_str)
                continue

            img = dcm.pixel_array.astype('float')
            mask = self._reshape_mask(mask, dcm)
            img, label = self._preprocess(img, dcm, mask)

            if self.context_type == 'temporal':
                img_fname = os.path.join(path, fpath)
                img = self._add_context_frames(img_fname, img)

            elif self.context_type == 'spacial':
                # TODO
                pass

            self.data[patient][sample_type]['images'].append(img)
            self.data[patient][sample_type]['labels'].append(label)
            self.data[patient][sample_type]['dcm'].append(dcm)
            self.img_counts[sample_type] += 1

        n = len(self.data[patient][sample_type]['images'])
        if n > 0:
            target_shape = (n, self.crop_size, self.crop_size, -1)
            reshaped = np.array(self.data[patient][sample_type]['images']).reshape(*target_shape)
            self.data[patient][sample_type]['images'] = reshaped
            reshaped = np.array(self.data[patient][sample_type]['labels']).reshape(*target_shape)
            self.data[patient][sample_type]['labels'] = reshaped


    def _add_context_frames(self, labeled_frame_fname, labeled_frame):
        
        m1_dcm = dicom.read_file(labeled_frame_fname + '_m1')
        p1_dcm = dicom.read_file(labeled_frame_fname + '_p1')
        m1_img = m1_dcm.pixel_array.astype('float')
        p1_img = p1_dcm.pixel_array.astype('float')
        m1_img, _ = self._preprocess(m1_img, m1_dcm, None)
        p1_img, _ = self._preprocess(p1_img, p1_dcm, None)
        
        return np.dstack((m1_img, labeled_frame, p1_img))

    
    def write(self, out_path, mode='split', ftype='numpy', split=[0.75, 0.15, .10], overwrite=False):
        
        if mode == 'patients':
            self._write_patient_dirs(out_path, ftype)
        elif mode == 'split':
            self._write_split_dirs(out_path, ftype, split, overwrite)
            
        
    
    def _write_patient_dirs(self, out_path, ftype='hdf5'):
        
        for patient in self.data:
            
            patient_out_dir = os.path.join(out_path, patient)
           
            if os.path.exists(patient_out_dir):
                shutil.rmtree(patient_out_dir)
            os.makedirs(patient_out_dir)
            
            for sample_type in ['ED', 'ES']:
        
                for idx in range(len(self.data[patient][sample_type]['images'])):

                    if ftype == 'hdf5':
                        fname = '{}_{}_{}.hdf5'.format(patient, sample_type, idx)
                        fpath = os.path.join(patient_out_dir, fname)
                        print(fpath)

                        fout = h5py.File(fpath, 'w')                    
                        fout.create_dataset('image', data=self.data[patient][sample_type]['images'][idx])
                        fout.create_dataset('label', data=self.data[patient][sample_type]['labels'][idx])
                        fout.create_dataset('fpath', data=fpath)
                        fout.close()
                        
                    elif ftype == 'numpy':
                        img = np.array(self.data[patient][sample_type]['images'][idx])
                        label = np.array(self.data[patient][sample_type]['labels'][idx])

                        img_fname = '{}_{}_{}_image.npy'.format(patient, sample_type, idx)
                        label_fname = '{}_{}_{}_mask.npy'.format(patient, sample_type, idx)
                        print("{}\t{}".format(img_fname, label_fname))

                        np.save(os.path.join(patient_out_dir, img_fname), img)                    
                        np.save(os.path.join(patient_out_dir, label_fname), label)
                   
                    
                    
    def _write_split_dirs(self, out_path, ftype='numpy', split=[0.75, .15, .10], overwrite=False):
        
        try:
            self._prep_split_dirs(out_path, overwrite)
        except:
            self._print('_write_split_dirs: exiting')
            
        counts = {'train': 0, 'val': 0, 'test': 0}
        N = self.img_counts['ED'] + self.img_counts['ES']
        n_train = int(split[0] * N)
        n_val = int(split[1] * N)
        self._print(N, n_train, n_val)
        
        for patient in self.data:
            
            if counts['train'] < n_train:
                split_dir = 'train'
            elif counts['val'] < n_val:
                split_dir = 'val'
            else:
                split_dir = 'test'
                
            patient_out_dir = os.path.join(out_path, split_dir)
      
            for sample_type in ['ED', 'ES']:
                images = self.data[patient][sample_type]['images']
                labels = self.data[patient][sample_type]['labels']
                
                for idx in range(len(images)):
                    img = np.array(images[idx])
                    label = np.array(labels[idx])
                    
                    img_fname = '{}_{}_{}_image.npy'.format(patient, sample_type, idx)
                    label_fname = '{}_{}_{}_mask.npy'.format(patient, sample_type, idx)
                    print("{}\t{}".format(img_fname, label_fname))
                       
                    np.save(os.path.join(patient_out_dir, img_fname), img)                    
                    np.save(os.path.join(patient_out_dir, label_fname), label)
                    counts[split_dir] += 1
                   
                
    def show_sample_img(self, patient=None, sample_type=None, i=None):
        p = patient if patient else np.random.choice(list(self.data.keys()))
        h = sample_type if sample_type else 'ED'
        i = i if i else np.random.randint(0, len(self.data[p][h]))
        img = self.data[p][h]['images'][i][:,:,0]
        ctr = self.data[p][h]['labels'][i][:,:,0]
        util.overlay_contour(img, ctr, pos=1, vis=True)
                           
    
    
    def _print(self, *s):
        if self.verbose:
            for c in s:
                print(c, end='')
            print()
            
            
if __name__ == '__main__':
    pass