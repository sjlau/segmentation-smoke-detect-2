import numpy as np


def CheckImageFitsInPatch(image, roi, max_radius, patch_size):
    boFlag = True
    max_radius += max_radius*.05
    if (max_radius > patch_size[0]/2) or (max_radius > patch_size[1]/2)\
        or (image.shape[0]>=512) or (image.shape[1]>=512):
        # print('The patch wont fit the roi: resize image', image.shape, max_radius, patch_size[0]/2)
        boFlag = False
    return boFlag

def crop_img_patch_from_roi(image_2D, roi_center, patch_size=(128,128)):
    """
    This code extracts a patch of defined size from the given center point of the image
    and returns parameters for padding and translation to original location of the image
    Args:
    2D: Volume:  Y, X
    """

    cols, rows = image_2D.shape
    patch_cols = np.uint16(max(0, roi_center[0] - patch_size[0]/2))
    patch_rows = np.uint16(max(0, roi_center[1] - patch_size[0]/2))
    # print (patch_cols,patch_cols+patch_size[0], patch_rows,patch_rows+patch_size[0])
    patch_img = image_2D[patch_cols:patch_cols+patch_size[0], patch_rows:patch_rows+patch_size[0]]
    return patch_img

def extract_patch(image_2D, roi_center, patch_size=(128, 128)):
    """
    This code extracts a patch of defined size from the given center point of the image
    and returns parameters for padding and translation to original location of the image
    Args:
    2D: Volume: X ,Y
    """
    cols, rows = image_2D.shape

    patch_cols = np.uint16(max(0, roi_center[0] - patch_size[0]/2))
    patch_rows = np.uint16(max(0, roi_center[1] - patch_size[0]/2))
    patch_img = image_2D[patch_cols:patch_cols+patch_size[0], patch_rows:patch_rows+patch_size[0]]
    if patch_img.shape != patch_size:
        # Pad the image with appropriately to patch_size
        print ('Not supported yet: Patch_size is bigger than the input image')
    # patch_img = np.expand_dims(patch_img, axis=1)
    pad_params = {'rows': rows, 'cols': cols, 'tx': patch_rows, 'ty': patch_cols}
    return patch_img, pad_params

def resize_image_with_crop_or_pad_3D(image, target_height, target_width):
    """Crops and/or pads an image to a target width and height.
    Resizes an image to a target width and height by either centrally
    cropping the image or padding it evenly with zeros.
    If `width` or `height` is greater than the specified `target_width` or
    `target_height` respectively, this op centrally crops along that dimension.
    If `width` or `height` is smaller than the specified `target_width` or
    `target_height` respectively, this op centrally pads with 0 along that
    dimension.
    Args:
    image: 3-D Tensor of shape `[ height, width, channels]` or
    target_height: Target height.
    target_width: Target width.
    Raises:
    ValueError: if `target_height` or `target_width` are zero or negative.
    Returns:
    Cropped and/or padded image.
    """

    # `crop_to_bounding_box` and `pad_to_bounding_box` have their own checks.
    def max_(x, y):
        return max(x, y)

    def min_(x, y):
        return min(x, y)

    def equal_(x, y):
        return x == y

    height, width, _ = image.shape
    width_diff = target_width - width
    offset_crop_width = max_(-width_diff // 2, 0)
    offset_pad_width = max_(width_diff // 2, 0)

    height_diff = target_height - height
    offset_crop_height = max_(-height_diff // 2, 0)
    offset_pad_height = max_(height_diff // 2, 0)

    # Maybe crop if needed.
    cropped = crop_to_bounding_box_3D(image, offset_crop_height, offset_crop_width,
                                 min_(target_height, height),
                                 min_(target_width, width))

    # Maybe pad if needed.
    resized = pad_to_bounding_box_3D(cropped, offset_pad_height, offset_pad_width,
                                target_height, target_width)
    return resized

def pad_to_bounding_box_3D(image, offset_height, offset_width, target_height,
                        target_width):
    """Pad `image` with zeros to the specified `height` and `width`.
    Adds `offset_height` rows of zeros on top, `offset_width` columns of
    zeros on the left, and then pads the image on the bottom and right
    with zeros until it has dimensions `target_height`, `target_width`.
    This op does nothing if `offset_*` is zero and the image already has size
    `target_height` by `target_width`.
    Args:
    image: 2-D Tensor of shape `[height, width]`
    offset_height: Number of rows of zeros to add on top.
    offset_width: Number of columns of zeros to add on the left.
    target_height: Height of output image.
    target_width: Width of output image.
    Returns:
    If `image` was a 3-D float Tensor of shape
    `[target_height, target_width, channels]`
    Raises:
    ValueError: If the shape of `image` is incompatible with the `offset_*` or
      `target_*` arguments, or either `offset_height` or `offset_width` is
      negative.
    """
    height, width, _ = image.shape

    after_padding_width = target_width - offset_width - width
    after_padding_height = target_height - offset_height - height

    assert (offset_height >= 0),"offset_height must be >= 0"    
    assert (offset_width >= 0),"width must be <= target - offset"    
    assert (after_padding_width >= 0),"height must be <= target - offset"    
    assert (after_padding_height >= 0),"height must be <= target - offset"    

    # Do not pad on the depth dimensions.
    padded = np.lib.pad(image, ((offset_height, after_padding_height),
                     (offset_width, after_padding_width), (0, 0)), 'constant')
    return padded


def crop_to_bounding_box_3D(image, offset_height, offset_width, target_height,
                         target_width):
    """Crops an image to a specified bounding box.
    This op cuts a rectangular part out of `image`. The top-left corner of the
    returned image is at `offset_height, offset_width` in `image`, and its
    lower-right corner is at
    `offset_height + target_height, offset_width + target_width`.
    Args:
    image: 3-D Tensor of shape `[height, width, channels]`.
    offset_height: Vertical coordinate of the top-left corner of the result in
                   the input.
    offset_width: Horizontal coordinate of the top-left corner of the result in
                  the input.
    target_height: Height of the result.
    target_width: Width of the result.
    Returns:
    If `image` was  a 3-D float Tensor of shape
    `[target_height, target_width, channels]`
    Raises:
    ValueError: If the shape of `image` is incompatible with the `offset_*` or
      `target_*` arguments, or either `offset_height` or `offset_width` is
      negative, or either `target_height` or `target_width` is not positive.
    """
    height, width, _ = image.shape

    assert (offset_width >= 0),"offset_width must be >= 0."    
    assert (offset_height >= 0),"offset_height must be >= 0."    
    assert (target_width > 0),"target_width must be > 0."    
    assert (target_height > 0),"target_height must be > 0." 
    assert (width >= (target_width + offset_width)),"width must be >= target + offset."    
    assert (height >= (target_height + offset_height)),"height must be >= target + offset."    
    cropped = image[offset_height: target_height+offset_height, offset_width: target_width+offset_width, :]
    return cropped


def pad_to_bounding_box(image, offset_height, offset_width, target_height,
                        target_width, pad_mode='symmetric'):
    """Pad `image` with zeros to the specified `height` and `width`.
    Adds `offset_height` rows of zeros on top, `offset_width` columns of
    zeros on the left, and then pads the image on the bottom and right
    with zeros until it has dimensions `target_height`, `target_width`.
    This op does nothing if `offset_*` is zero and the image already has size
    `target_height` by `target_width`.
    Args:
    image: 2-D Tensor of shape `[height, width]`
    offset_height: Number of rows of zeros to add on top.
    offset_width: Number of columns of zeros to add on the left.
    target_height: Height of output image.
    target_width: Width of output image.
    Returns:
    If `image` was 2-D, a 2-D float Tensor of shape
    `[target_height, target_width]`
    Raises:
    ValueError: If the shape of `image` is incompatible with the `offset_*` or
      `target_*` arguments, or either `offset_height` or `offset_width` is
      negative.
    """
    height, width = image.shape

    after_padding_width = target_width - offset_width - width
    after_padding_height = target_height - offset_height - height

    assert (offset_height >= 0),"offset_height must be >= 0"    
    assert (offset_width >= 0),"width must be <= target - offset"    
    assert (after_padding_width >= 0),"height must be <= target - offset"    
    assert (after_padding_height >= 0),"height must be <= target - offset"    

    # Do not pad on the depth dimensions.
    padded = np.lib.pad(image, ((offset_height, after_padding_height),
                     (offset_width, after_padding_width)), pad_mode)
    return padded


def crop_to_bounding_box(image, offset_height, offset_width, target_height,
                         target_width):
    """Crops an image to a specified bounding box.
    This op cuts a rectangular part out of `image`. The top-left corner of the
    returned image is at `offset_height, offset_width` in `image`, and its
    lower-right corner is at
    `offset_height + target_height, offset_width + target_width`.
    Args:
    image: 2-D Tensor of shape `[height, width]`.
    offset_height: Vertical coordinate of the top-left corner of the result in
                   the input.
    offset_width: Horizontal coordinate of the top-left corner of the result in
                  the input.
    target_height: Height of the result.
    target_width: Width of the result.
    Returns:
    If `image` was 2-D, a 2-D float Tensor of shape
    `[target_height, target_width]`
    Raises:
    ValueError: If the shape of `image` is incompatible with the `offset_*` or
      `target_*` arguments, or either `offset_height` or `offset_width` is
      negative, or either `target_height` or `target_width` is not positive.
    """
    height, width = image.shape

    assert (offset_width >= 0),"offset_width must be >= 0."    
    assert (offset_height >= 0),"offset_height must be >= 0."    
    assert (target_width > 0),"target_width must be > 0."    
    assert (target_height > 0),"target_height must be > 0." 
    assert (width >= (target_width + offset_width)),"width must be >= target + offset."    
    assert (height >= (target_height + offset_height)),"height must be >= target + offset."    
    cropped = image[offset_height: target_height+offset_height, offset_width: target_width+offset_width]
    return cropped

def resize_image_with_crop_or_pad(image, target_height, target_width, pad_mode='symmetric'):
    """Crops and/or pads an image to a target width and height.
    Resizes an image to a target width and height by either centrally
    cropping the image or padding it evenly with zeros.
    If `width` or `height` is greater than the specified `target_width` or
    `target_height` respectively, this op centrally crops along that dimension.
    If `width` or `height` is smaller than the specified `target_width` or
    `target_height` respectively, this op centrally pads with 0 along that
    dimension.
    Args:
    image: 2-D Tensor of shape `[ height, width]` or
    target_height: Target height.
    target_width: Target width.
    Raises:
    ValueError: if `target_height` or `target_width` are zero or negative.
    Returns:
    Cropped and/or padded image.
    """

    # `crop_to_bounding_box` and `pad_to_bounding_box` have their own checks.
    def max_(x, y):
        return max(x, y)

    def min_(x, y):
        return min(x, y)

    def equal_(x, y):
        return x == y

    height, width = image.shape
    width_diff = target_width - width
    offset_crop_width = max_(-width_diff // 2, 0)
    offset_pad_width = max_(width_diff // 2, 0)

    height_diff = target_height - height
    offset_crop_height = max_(-height_diff // 2, 0)
    offset_pad_height = max_(height_diff // 2, 0)

    # Maybe crop if needed.
    cropped = crop_to_bounding_box(image, offset_crop_height, offset_crop_width,
                                 min_(target_height, height),
                                 min_(target_width, width))

    # Maybe pad if needed.
    resized = pad_to_bounding_box(cropped, offset_pad_height, offset_pad_width,
                                target_height, target_width, pad_mode)

    return resized


def roi_crop(image, label, roi, roi_radii, max_size=(180, 180), patch_size=(180, 180)):

    max_radius = roi_radii[1]
    max_size = (180, 180)
    patch_size = (180, 180)
    
    image, label = np.array(image), np.array(label)

    if CheckImageFitsInPatch(image, roi, max_radius, patch_size):
        # Center around roi
        patch_image = crop_img_patch_from_roi(image, roi, patch_size)
        patch_label = crop_img_patch_from_roi(label, roi, patch_size)
        # If patch size does not fit then pad or crop
        patch_image = resize_image_with_crop_or_pad_3D(patch_image[:,:, None], patch_size[0], patch_size[1])
        patch_label = resize_image_with_crop_or_pad_3D(patch_label[:,:, None], patch_size[0], patch_size[1])
        # print (patch_image.shape, patch_label.shape)
    else:
        patch_image  = crop_img_patch_from_roi(image, roi, max_size)
        patch_label = crop_img_patch_from_roi(label, roi, max_size)
        patch_image = resize_sitk_2D(patch_image, patch_size)
        patch_label = resize_sitk_2D(patch_label, patch_size, interpolator=sitk.sitkNearestNeighbor)
        
    return patch_image, patch_label