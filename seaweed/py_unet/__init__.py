"""
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell
Copyright © 2019 The Regents of the University of California 
Please see LICENSE.txt for complete license information. 
"""
from tensorflow import ConfigProto
from tensorflow import Session 
from tensorflow.python.client import device_lib
from keras.models import *
from keras.utils import multi_gpu_model


class ModelMGPU(Model):
    """ The based model is passed in for running the process with multi gpus when possible 
    Taken from https://github.com/keras-team/keras/issues/2436#issuecomment-354882296
    """
    def __init__(self, ser_model, gpus):
        pmodel = multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        """ Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model. """
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)
    
    

def device_config(verbose=True):
    """ Configure GPU settings. Will run on as many as are available """
    
    if verbose:
        print(device_lib.list_local_devices())
        
    n_gpus = 0
    for dev in device_lib.list_local_devices():
        if 'gpu' in dev.device_type.lower():
            n_gpus += 1

    if n_gpus > 0:
        gpu_devices = ','.join([str(i) for i in range(n_gpus)])
        os.environ['CUDA_VISIBLE_DEVICES'] = gpu_devices

        config = ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.4
        session = Session(config=config)
        K.set_session(session)

    return n_gpus
