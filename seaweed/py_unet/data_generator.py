"""
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.
"""
import sys
import os
import warnings
import numpy as np
from keras.utils import Sequence
from sklearn.utils import shuffle
import itertools
import random
import matplotlib.pyplot as plt

from .batch_augmentor import BatchAugmentor
from .utils import plot_utils as ph



class DataGenerator(Sequence):

    
    def __init__(self, 
                 directories, 
                 batch_size=4, 
                 image_dims=(176,176), 
                 n_channels=1, 
                 shuffle=True, 
                 p_sample=None, 
                 use_augmentation=False, 
                 aug_fn_args=None, 
                 n_aug_copies=1, 
                 **aug_kwargs):
     
        self.batch_size = batch_size
        self.image_dims = image_dims
        self.n_channels = n_channels
        self.shuffle = shuffle 

        self._image_fnames = []
        self._mask_fnames = []  
        
        self._load_filenames(directories)
        
        if p_sample:
            self._sample_filenames(p_sample)

        notempty = lambda l: (l is not None and len(l) > 0)
        if use_augmentation and notempty(aug_fn_args):
            self._allow_augmentation = True
            self._n_aug_copies = n_aug_copies   
            self._add_dummy_aug_files(self._n_aug_copies, 
                                      list(aug_fn_args.keys()))                        
            self._augmentor = BatchAugmentor(aug_fn_args,
                                             self.batch_size,  
                                             **aug_kwargs)
        else:
            if notempty(aug_fn_args):
                warn_str = "You passed in data augmentation parameters,"
                warn_str += "but use_augmentation is set to False."
                warnings.warn(warn_str)
            self._allow_augmentation = False           

        if self.shuffle == True:
            self._shuffle()
     
        self._epoch = 0
        self._batch_num = None
        self._get_batch_indices = lambda i: np.arange(i*self.batch_size, 
                                                      (i+1)*self.batch_size)
        
        self._is_cached = lambda idx: idx == self._cache['batch_num']
        self._cache = {'batch_num': None, 'X': None, 'Y': None, 
                       'filenames': None, 'augmentation': None}

        npy_fname = lambda f: ''.join(f.split('.aug')[:-1]) + '.npy'
        self._fname = lambda f: npy_fname(f) if '.aug' in f else  f
        
        self.on_epoch_end()
        
        disp_str = 'Finished loading: {} batches, {} samples'
        print(disp_str.format(self.__len__(), self.n_samples))
        
    
    def on_epoch_end(self):
        """ Called by Keras at end of epoch """
        self._epoch += 1
        if self.shuffle == True:
            self._shuffle()
                   
    @property
    def n_samples(self):
        return len(self._image_fnames)
        
        
    def __len__(self):
        """ Number of batches in this generator """
        return int(np.floor(self.n_samples / self.batch_size))

        
    def get_metadata(self, image_filepath):
        """ Load the metadata associated with an image """
        return np.load(image_filepath.replace('_image.npy', '_metadata.npy')).item()
    
    
    def __getitem__(self, index):
        """ Get a single batch of data """
    
        if self._is_cached(index):
            return self._cache['X'], self._cache['Y']
        
        batch_indices = self._get_batch_indices(index)
        batch_fnames = [self._image_fnames[i] for i in batch_indices]
        batch_ops = None    
        
        X = np.empty((self.batch_size, *self.image_dims, self.n_channels))        
        Y = np.empty((self.batch_size, *self.image_dims, 1)) if self._labels_present else None
        
        # Load from numpy files    
        for i, idx in enumerate(batch_indices):
            X[i,] = np.load(self._fname(self._image_fnames[idx]))
            if self._labels_present:
                Y[i,] = np.load(self._fname(self._mask_fnames[idx]))
        
        # Optional Augmentation            
        if self._allow_augmentation:
            X, Y, batch_ops = self._augment_batch(X, Y, batch_fnames)
       
        self._cache_batch(X, Y, index, batch_fnames, batch_ops)
        return X, Y
    

    def _cache_batch(self, X, Y, index, batch_fnames, batch_ops):
        """ Cache for easy retrieval """
        self._cache = {
            'X': X, 
            'Y': Y, 
            'batch_num': index,
            'augmentations': batch_ops,
            'filenames': batch_fnames
        }

        
    def _augment_batch(self, X, Y, batch_fnames):
        """ Apply the augmentations specified in the dummy filenames of the 
        batch 
        """
        
        identity = lambda f: '.aug' not in f
        aug_name = lambda f: f.split('/')[-1].split('.aug')[-1]
        get_aug_name = lambda f: 'random' if aug_name(f) == '' else aug_name(f)[1:]        
        batch_augs = [None if identity(fname) else get_aug_name(fname) 
                      for fname in batch_fnames]
        
        X, Y, batch_ops = self._augmentor.process_batch(X, Y, 
                                                        apply_batch_augs=batch_augs,
                                                        prev_batch_augs=None) 
        return X, Y, batch_ops
        
        
    def _shuffle(self):        
        """ """
        if self._labels_present:
            self._image_fnames, self._mask_fnames = shuffle(self._image_fnames, self._mask_fnames)
        else:
            self._image_fnames = shuffle(self._image_fnames)
      
        
    def _load_filenames(self, directories):
        """ """
        if isinstance(directories, str): 
            directories = [directories]
        
            
        # Get filepaths from all dirs, flatten into one list
        full_paths = lambda d, d_ls: [os.path.join(d, f) for f in d_ls]
        fnames = [full_paths(d, os.listdir(d)) for d in directories]
        fnames = list(itertools.chain.from_iterable(fnames))
        
        name = lambda s: s.split('/')[-1]
        self._image_fnames = sorted([f for f in fnames if 'image' in name(f)], key=name)
        self._mask_fnames = sorted([f for f in fnames if 'mask' in name(f)], key=name)
        self._labels_present = len(self._mask_fnames) > 0
        
        err_str = 'Number of images ({}) not equal to number of masks ({})'
        n_images, n_masks = len(self._image_fnames), len(self._mask_fnames)
        if self._labels_present:
            assert n_images == n_masks, err_str.format(n_images, n_masks)

       
    def _sample_filenames(self, p_sample):
        """ Take a random sample from the filenames """
        indices = random.sample(range(self.n_samples), int(self.n_samples * p_sample))
        self._image_fnames = [self._image_fnames[i] for i in indices]
        if self._labels_present:
            self._mask_fnames = [self._mask_fnames[i] for i in indices]        
  

    def _add_dummy_aug_files(self, n_aug_copies, augmentations):
        """ 
        Add dummy filenames to indicate augmented copies of originals 
        """    

        aug_image_fnames = [f.replace('.npy', '.aug') for f in self._image_fnames]
        aug_mask_fnames = [f.replace('.npy', '.aug') for f in self._mask_fnames]            

        randomize_augs = n_aug_copies != len(augmentations)

        if n_aug_copies > 0:
            for i in range(n_aug_copies):
                if not randomize_augs:
                    aug_name_ext = '.{}'.format(augmentations[i])
                    image_dummies = [f + aug_name_ext for f in aug_image_fnames]
                    mask_dummies = [f + aug_name_ext for f in aug_mask_fnames]
                else:
                    image_dummies = aug_image_fnames
                    mask_dummies = aug_mask_fnames        

                self._image_fnames += image_dummies
                self._mask_fnames += mask_dummies  
        else:
            self._image_fnames = aug_image_fnames
            self._mask_fnames = aug_mask_fnames              
            
    
    def load_all(self):
        """ Only use for small datasets pls """
        
        fnames = []        
        X = np.empty((self.n_samples, *self.image_dims, self.n_channels))
        Y = np.empty((self.n_samples, *self.image_dims, 1)) if self._labels_present else None
        
        for i in range(self.__len__()):      
            batch_indices = self._get_batch_indices(i)
            files = [(self._image_fnames[j],) for j in batch_indices]
            
            X_batch, Y_batch = self.__getitem__(i)
            X[batch_indices,] = X_batch
            if self._labels_present:    
                Y[batch_indices,] = Y_batch
                files = [f + (self._mask_fnames[j],) for j, f in enumerate(files)]

            fnames.append(files)        

        return {'X': X, 'Y': Y, 'filenames': fnames}
    
    
    def show_batch(self, index=0, overlay_masks=True, nrows=1, **kwargs):
        """ Displays a batch as a grid of images """
        
        assert self.batch_size % nrows == 0, 'batch_size % nrows must be 0'
        assert nrows < self.batch_size, 'nrows can\'t exceed batch_size'

        self.__getitem__(index)
        X = self._cache['X']
        Y = self._cache['Y']
        augs = self._cache['augmentations']        
        fnames = self._cache['filenames']  
  
        separate_masks = self._labels_present and not overlay_masks
        row_step = 2 if separate_masks else 1
        
        ncols = self.batch_size // nrows
        if separate_masks:
            nrows *= 2
                
        patient_name = lambda f: f.split('/')[-1].split('_image')[0]
        aug_name = lambda i: '\n' + augs[i] if augs[i] else ''                
               
        figsize = (7*self.batch_size, 10*nrows)
        imgsize = (figsize[0]/nrows, figsize[1]/ncols)
        
        plt.gray()  
        fig = plt.figure(figsize=figsize, constrained_layout=True)
        grid = plt.GridSpec(nrows, ncols, fig)#, hspace=0.1, wspace=0.1)      
        fig.suptitle('Batch {} / {}'.format(index, self.__len__()), fontsize=24)
        
        for r in range(0, nrows, row_step):
            for c in range(ncols):
                title = patient_name(fnames[r+c][0])
                title += aug_name(r+c) if augs else ''                
                ax = fig.add_subplot(grid[r, c])
                ax.set_title(title, fontsize=32)
                ax.set_yticks([])
                ax.set_xticks([])               
                img = X[r+c,:,:,0]
                if self._labels_present:
                    mask = Y[r+c,:,:,0]
                    if overlay_masks:
                        img_masked = ph.overlay_contour(img, mask, vis=False)
                        ax.imshow(img_masked, aspect='equal')
                    else:
                        ax.imshow(img, aspect='equal')
                        fig.add_subplot(grid[r+1, c]).imshow(mask, aspect='equal')
                else:
                    ax.imshow(img, aspect='equal')
        if 'savefig' in kwargs:
            plt.savefig(kwargs['savefig'], dpi=400)
                    
        plt.show();
        
    def _retrieve(self, image_fname):

        idx = self._image_fnames.index(image_fname)
        X = np.load(self._fname(self._image_fnames[idx]))
        if self._labels_present:
            Y = np.load(self._fname(self._mask_fnames[idx]))
        return X, Y
