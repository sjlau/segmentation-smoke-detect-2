"""                                                         
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell 
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.    
"""
from keras.models import model_from_json
from keras import backend as K
import numpy as np
import pickle
import h5py
import json
import os

from . import plot_utils as ph
from .. import ModelMGPU



def get_inner_model(model):
    if not isinstance(model, ModelMGPU):
        return model
    for layer in model.layers:
        if 'model_' in layer.name:
            return layer
    raise KeyError("No model layer found")


def save_model_state(model, model_name, model_dir):

    w_fpath = os.path.join(model_dir, '{}_weights.h5'.format(model_name))
    opt_fpath = os.path.join(model_dir, '{}_optimizer.pkl'.format(model_name))
    
    if isinstance(model, ModelMGPU):
        inner_model = get_inner_model(model)
        inner_model.save_weights(w_fpath)
    else:
        model.save_weights(w_fpath)
    
    symbolic_weights = getattr(model.optimizer, 'weights')
    weight_values = K.batch_get_value(symbolic_weights)
    with open(opt_fpath, 'wb') as f:
        pickle.dump(weight_values, f)   

        
def save_model(model, model_name, model_dir):

    model_fpath = os.path.join(model_dir, '{}.json'.format(model_name))    
    w_fpath = os.path.join(model_dir, '{}_weights.h5'.format(model_name))
    opt_fpath = os.path.join(model_dir, '{}_optimizer.pkl'.format(model_name))
    
    json.dump(model.to_json(), open(model_fpath, 'w'))
    
    if isinstance(model, ModelMGPU):
        inner_model = get_inner_model(model)
        inner_model.save_weights(w_fpath)
    else:
        model.save_weights(w_fpath)
    
    symbolic_weights = getattr(model.optimizer, 'weights')
    weight_values = K.batch_get_value(symbolic_weights)
    with open(opt_fpath, 'wb') as f:
        pickle.dump(weight_values, f)           
        
        
def load_model_state(model, model_name, model_dir):

    w_fpath = os.path.join(model_dir, '{}_weights.h5'.format(model_name))
    opt_fpath = os.path.join(model_dir, '{}_optimizer.pkl'.format(model_name))

    with h5py.File(w_fpath, 'r') as w_file:
        for layer_key in list(w_file.keys()):
            if 'model_' in layer_key:
                model = load_mgpu_weights(model, w_file['model_1'])
        else:
            model.load_weights(w_fpath)
        
    model._make_train_function()

    
    try:
        with open(opt_fpath, 'rb') as f:
            weight_values = pickle.load(f)
        model.optimizer.set_weights(weight_values)
    except (IOError, ValueError) as err:
        print("Warning: Couldn't load optimizer")
        print(err)
     
    return model


def load_model(model_name, model_dir, opt, loss=None, metrics=None):
    
    model_fpath = os.path.join(model_dir, '{}.json'.format(model_name))
    w_fpath = os.path.join(model_dir, '{}_weights.h5'.format(model_name))
    opt_fpath = os.path.join(model_dir, '{}_optimizer.pkl'.format(model_name))

    model = model_from_json(json.load(open(model_fpath, 'r')))
    
    with h5py.File(w_fpath, 'r') as w_file:
        for layer_key in list(w_file.keys()):
            if 'model_' in layer_key:
                model = load_mgpu_weights(model, w_file['model_1'])
        else:
            model.load_weights(w_fpath)
            
    model.compile(optimizer=opt, 
                  loss=loss,  
                  metrics=metrics)
    #model._make_train_function()
   
    try:
        with open(opt_fpath, 'rb') as f:
            weight_values = pickle.load(f)
        model.optimizer.set_weights(weight_values)
    except (IOError, ValueError) as err:
        print("Warning: Couldn't load optimizer")
        print(err)
    
    return model

    
def load_mgpu_weights(model, h5_weights):
    """ """
    if 'model_1' in h5_weights:
        h5_weights = h5_weights["model_1"]
    
    for layer in model.layers:
        if layer.name in weight_file:
            layer_weights = h5_weights[layer.name]
            try:
                weights = []
                # Extract weights
                for term in layer_weights:
                    print(term, type(layer_weights[term]))
                    if isinstance(layer_weights[term], h5py.Dataset):
                        # Convert weights to numpy array and prepend to list
                        weights.insert(0, np.array(layer_weights[term]))

                # Load weights to model
                layer.set_weights(weights)

            except Exception as e:
                err_str = "Error: Could not load weights for layer:",
                print(err_str, layer.name, file=stderr)
    return model

        
def freeze_unet_blocks(model, freeze_blocks=range(0,8), verbose=True):
    
    model = get_inner_model(model)
    curr_block = 0
    if verbose:
        print('--- Block {} ---'.format(curr_block))
    
    for layer in model.layers:
                    
        if curr_block in freeze_blocks:
            layer.trainable = False       
            if verbose: 
                print("{}".format(layer.name).ljust(25, ' ') + "--\ttrainable = False")
        else:
            if verbose: 
                print("{}".format(layer.name).ljust(25, ' ') + "--\ttrainable = True")
            
        if 'max_pooling' in layer.name or 'up_sampling' in layer.name:
            curr_block += 1
            if verbose:
                print('\n--- Block {} ---'.format(curr_block))
        
    return model


def step_decay_fn(step=15, initial_value=0.001, factor=5, min_lr=0.00001):

    def fn(epoch):
        lr = initial_value
        if isinstance(factor, dict):
            # This overrides the step param
            for e in sorted(factor.keys()):
                if epoch >= e:
                    lr = lr/factor[e]
                    break
        else:
            num = epoch // step
            if num > 0:
                lr = lr/(factor*num)

        lr = np.clip(lr, min_lr, None).astype(np.float)
        print('Learning rate for epoch {} is {}.'.format(epoch+1, lr))
        return lr
    
    return fn

