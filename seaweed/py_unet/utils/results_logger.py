"""                                                         
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell 
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.    
"""
from collections import defaultdict
from datetime import datetime
import numpy as np
import pickle
import os



class ResultsLogger:


    def __init__(self, name, out_dir='results', load=True):
        """
        Keeps a dictionary where keys are metric names and values are lists
        of metric values from a series of experiments
        """
        self.name = name
        self.out_dir = out_dir
        self._metrics = defaultdict(list)
        self._loaded = False
        if load:
            self.load()
               
        
    def __getitem__(self, x):
        return self._metrics[x]
    
    
    def add(self, metric, value):
        self._metrics[metric].append(value)
        
        
    def clear(self, idx=None, metrics=[], write_out=True):
        
        if idx is not None:
            if not metrics:
                metrics =  list(self._metrics.keys())
            if isinstance(idx, int):
                idx = [idx]
            for i in idx:
                for m in self._metrics:
                    del self._metrics[m][i]
              
        else:
            if metrics:
                for m in metrics:
                    self._metrics[m] = []
            else:
                self._metrics = defaultdict(list)
                self._loaded = False
            
        if write_out:
            self.write(False)
       
    
    def load(self):
        
        if not self._loaded:
            fpath = os.path.join(self.out_dir, '{}_results.pkl'.format(self.name))
            if os.path.exists(fpath):
                print("ResultsLogger: loading from {}".format(fpath))               
                self._metrics = pickle.load(open(fpath, 'rb'))
                self._loaded = True
            else:
                info_str = "ResultsLogger: No existing log file "
                info_str += "(logger.write() will create {})"
                print(info_str.format(fpath))
        
        
    def write(self, append=True):
        
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)

        fpath = os.path.join(self.out_dir, '{}_results.pkl'.format(self.name))

        if append and os.path.exists(fpath) and not self._loaded:
            self.load()
      
        pickle.dump(self._metrics, open(fpath, 'wb')) 
        
        
    def show(self, metric=None):
        print('----- {} -----\n'.format(self.name))
        if metric:
            print('{}: {}'.format(metric, self._metrics[metric]))
        else:
            for metric in self._metrics:
                print('{}: {}'.format(metric, self._metrics[metric]))
        print('\n-------------------------')
        
        
