"""                                                         
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell 
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.    
"""
from keras import backend as K
from keras.losses import binary_crossentropy
from keras.layers import Lambda



def dice_coef(y_true, y_pred, smooth=1):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_loss(y_true, y_pred):
    return 1. - dice_coef(y_true, y_pred)


def bce_dice_loss(y_true, y_pred):
    return binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)

                    
def calc_dice(tp, fp, fn, smooth=0.001):
    return (2*tp) /(2*tp + fp + fn + smooth)
            
    
def calc_precision(tp, fp, smooth=0.001):
    return tp / (tp + fp + smooth)
            
    
def calc_recall(tp, fn, smooth=0.001):
    return tp / (tp + fn + smooth)    

