"""                                                         
Copyright © 2019 Dylan Uys, Mai Nguyen, & Garrison Cottrell 
Copyright © 2019 The Regents of the University of California
Please see LICENSE.txt for complete license information.    
"""
import sys
sys.path.append('../kerassurgeon')
import kerassurgeon.utils as ksutils    
from kerassurgeon import identify
from kerassurgeon.operations import delete_channels
from keras.utils import Sequence
from keras import models
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.cm import ScalarMappable
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from collections import defaultdict, OrderedDict
import inspect
import numpy as np

from py_unet.vis.visualization import saliency


class UnetVisualizer:
    """ Class for visualizing activations in a U-Net
    Creates a new model that has outputs for every layer in the
    passed model, which we call the activation model. 

    args:
        model - A trained model whose weights you want to visualize
        data - Either a generator (keras.utils.Sequence instance) or 
               a dictionary with the key 'X' pointing to a Numpy matrix
               of inputs and the key 'Y' for the associated labels. The
               model will be applied to this data to get activations. 
               If data not provided here, you must call activate(data) prior
               to visualizing anything.
        verbose - True for informational print statements
    """   
    
    def __init__(self,
                 model, 
                 seed_data=None, 
                 max_cols=8, 
                 default_cmap='viridis',                 
                 normalize_colors=True,
                 lognorm_colors=False,
                 verbose=2):

        self._verbose = verbose  
        self._max_cols = max_cols
        self._cmap = default_cmap         
        self._normalize_colors = normalize_colors
        self._lognorm_colors = lognorm_colors
        
        self._zeroish = 1e-8
        self._nchannels = lambda name: self._activations[name].shape[-1]
        
        self._seed_data = seed_data
        self.model = model
        
        
        self._vis_fns = {
            'features': {'sequential': self._visualize_feature_maps,
                         'fusing': self._visualize_feature_maps_fusing},
            'saliency': {'sequential': self._visualize_saliency,
                         'fusing': self._visualize_saliency_fusing}
        }        
        
        self._activations = {}       
        if seed_data is not None:
            self.activate(seed_data)
            
        if verbose > 1:
            self.model_summary()
        
        
    def activate(self, data):
        """ Apply the activation model to a dataset to get activated weights,
        which get stored in a dictionary by layer name for easy lookup.
        self._act_model is deleted afterwards, as it is no longer needed """

        act_model = self._get_activation_model(self.model)
        self._init_dict_attrs()  
        
        self._print('Getting activations...')        
        if isinstance(data, Sequence):
            activations = act_model.predict_generator(data)    
        else:
            data = data['X'] if isinstance(data, dict) else data
            activations = act_model.predict(data)
            self._seed_data = data
        
        for layer_act, layer_name in zip(activations, self._model_layer_names):
            self._activations[layer_name] = layer_act  
            
            
    def visualize(self, 
                  blocks=[],  
                  mode='features', 
                  order=None,
                  layer_types=[],                   
                  block_layer_idx=[],                  
                  **kwargs):
        
        if len(block_layer_idx)==0 and len(layer_types)==0:
            layer_types = ['conv', 'activation']

        list_mutex = lambda l1, l2: (len(l1)==0) != (len(l2)==0)
        mutex_err = "block_layer_idx and layer_types are mutually exclusive"
        assert list_mutex(block_layer_idx, layer_types), mutex_err  
            
        is_tup = lambda x: isinstance(x, tuple)
        is_list = lambda x: isinstance(x, list)
        is_int = lambda x: isinstance(x, int)
        is_int_list = lambda x: is_list(x) and len(x) > 0 and all([is_int(e) for e in x])
        is_tup_list = lambda x: is_list(x) and len(x) > 0 and  all([is_tup(e) for e in x])
        
        if not order: 
            if not blocks:
                order = 'sequential'
            else:
                if is_int(blocks) or is_int_list(blocks):
                    order = 'sequential'
                if is_tup(blocks) or is_tup_list(blocks):
                    order = 'fusing'
            
        if order == 'sequential':
            if not blocks:
                blocks = list(np.arange(0, self._nblocks))
            elif is_int(blocks):
                blocks = [blocks]
            elif not is_int_list(blocks):
                err = "'blocks' must be an int, list of ints, or None"
                raise ValueError("In 'sequential' order, " + err)       

        elif order == 'fusing':
            if not blocks:           
                blocks = self._all_block_tuples
            elif is_tup(blocks):
                blocks = [blocks]
            elif not (is_list(blocks) and all([is_tup(b) for b in blocks])):
                err = "'blocks' must be a tuple, list of tuples, or None"
                raise ValueError("In 'fusing' order, " + err)

        # Get vis function corresponding to mode and order params
        vis_fn = self._vis_fns[mode][order] 
        
        # Unpack keyword args into two dicts
        vis_fn_kwargs = self._set_fn_kwargs(vis_fn, kwargs)  
        kwargs = {k:kwargs[k] for k in set(kwargs) - set(vis_fn_kwargs)}

        # Call appropriate fn with all params filled (potentially with defaults)
        #vis_fn(blocks, layer_types, block_layer_idx, **vis_fn_kwargs, **kwargs)
        vis_fn(blocks, **vis_fn_kwargs, **kwargs)

    def model_summary(self):
        """ Print layers in order, organized into semantic blocks. 
        Show activation matrix shapes too, if available"""
        
        for block_id in self._block_layers:
            bottleneck = ''
            if block_id * 2 == self._last_block:
                bottleneck = ' (Bottleneck)'
                
            self._print('\n--- Block {} {}---'.format(block_id, bottleneck)) 
            for layer_name in self._block_layers[block_id]:
                shape = ''
                if len(self._activations) > 0:
                    shape = self._activations[layer_name].shape
                print('{}\t{}'.format(shape, layer_name))
                
        
    def _get_block_layers(self, block_id, layer_types=[]):
        
        layers = self._block_layers[block_id]   
        if layer_types:
            matches = lambda l, ltypes: [ltype in l for ltype in ltypes]            
            if not isinstance(layer_types, list):
                layer_types = [layer_types]
            layers = [l for l in layers if any(matches(l, layer_types))]
     
        return layers
    
                
    def _visualize_saliency(self, blocks, sep_filters=True, seed_data=None, **kwargs):
        
        for block in blocks:     
            act_layers = self._get_block_layers(block, 'activation')
            for layer_name in act_layers:        
                self._display_gradient_grid(layer_name, seed_data, sep_filters, **kwargs)

                
    def _visualize_saliency_fusing(self, block_tuples, sep_filters=True, seed_data=None, **kwargs):

        def show_saliency_grid(block_id, layer_idx):
            layer_name = self._get_block_layers(block_id, 'activation')[layer_idx]
            self._display_gradient_grid(layer_name, seed_data, sep_filters, **kwargs) 
                               
        for (ds_block, us_block) in block_tuples:           
            idx_fused = -1 if us_block != self._last_block else -2
            show_saliency_grid(us_block, 0)
            print('+')
            show_saliency_grid(ds_block, 0)
            print('=')   
            show_saliency_grid(us_block, idx_fused)
            if us_block == self._last_block:
                show_saliency_grid(us_block, -1)        

    
    def _visualize_feature_maps(self, blocks, **kwargs):
      
        for block in blocks:
            layers = [l for l in self._block_layers[block] 
                      if 'dropout' not in l]
            for layer_name in layers:
                self._display_feature_grid(layer_name, **kwargs)  
            
    
    def _visualize_feature_maps_fusing(self, block_tuples, filters=None, **kwargs):
        """ """
   
        def show_conv_act_grids(block_id, layer_idx):
            for ltype in ['conv', 'activation']:
                layer_name = self._get_block_layers(block_id, ltype)[layer_idx]
                self._display_feature_grid(layer_name, **kwargs)         

        for (ds_block, us_block) in block_tuples:           
            idx_fused = -1 if us_block != self._last_block else -2
            show_conv_act_grids(us_block, 0)
            print('+')
            show_conv_act_grids(ds_block, -1)
            print('=')           
            show_conv_act_grids(us_block, idx_fused)
            if us_block == self._last_block:
                show_conv_act_grids(us_block, -1)
    
        
    def _display_feature_grid(self, layer_name, **kwargs):

        grid_kwargs = self._set_fn_kwargs(self._generate_grid, kwargs)
        display_kwargs = self._set_fn_kwargs(self._display_grid, kwargs)  
        
        feat_maps = self._activations[layer_name]
        feat_grid, _ = self._generate_grid(layer_name, feat_maps, **grid_kwargs)
        self._display_grid(layer_name, 
                           feat_grid, 
                           fixed_scale=(feat_maps.shape[-1] == 1), 
                           **display_kwargs)                                                
 
        
        
    def _display_gradient_grid(self, layer_name, seed_data, sep_filters=True, **kwargs):

        grid_kwargs = self._set_fn_kwargs(self._generate_grid, kwargs)
        display_kwargs = self._set_fn_kwargs(self._display_grid, kwargs)  
                                              
        grad_map = self._get_layer_gradients(layer_name, seed_data, sep_filters)            
        grad_grid, seed_grid = self._generate_grid(layer_name, grad_map, **grid_kwargs)
        self._display_grid(layer_name, 
                           grad_grid, 
                           seed_grid, 
                           fixed_scale=(grad_map.shape[-1] == 1),
                           **display_kwargs)                   

        
    def _get_layer_gradients(self, layer_name, seed_data, sep_filters=True):
          
        layer_idx = self._model_layer_names.index(layer_name)
        gradients = np.zeros_like(self._activations[layer_name]) 
        
        if sep_filters:               
            filter_indices = list(range(gradients.shape[-1]))
        else:
            gradients = gradients.reshape(*gradients.shape[:-1])          
            filter_indices = [None]

        for i, filt in enumerate(filter_indices):
            grads = saliency.visualize_saliency(self.model, 
                                                layer_idx, 
                                                filter_indices=filt, 
                                                seed_input=seed_data,
                                                grad_modifier=None, 
                                                keepdims=True)
            gradients[0,:,:,i] = grads[:,:,0]        
        return gradients                 
    
    
    def _generate_grid(self, 
                       layer_name, 
                       channels, 
                       seed_data=None,
                       max_cols=None, 
                       normalize_colors=None):
        
        if isinstance(normalize_colors, list):
            normalize = lambda lname: any([ltype in lname for ltype in normalize_colors])
        elif isinstance(normalize_colors, bool):
            normalize = lambda _: normalize_colors
            
        n_features = channels.shape[-1]   
        dim = channels.shape[1]       
        
        n_cols = n_features if max_cols > n_features else max_cols
        n_rows = np.clip(n_features//n_cols, 1, None)     
       
        if seed_data is not None and seed_data.ndim > 2:
            seed_dim = seed_data.shape[1]
            seed_data = seed_data.reshape(seed_data.shape[1], seed_data.shape[2])
            layer_seed_grid = np.zeros((n_rows*seed_dim, n_cols*seed_dim))

            
        # Create empty grids, one for activations, one for optional seed image overlay
        layer_grid = np.zeros((n_rows*dim, n_cols*dim))

        # Iteratively add each filter to the grid
        for row in range(n_rows): 
            for col in range(n_cols):
                channel_index = row * n_cols + col
                channel = channels[0,:,:,channel_index]

                # Adjust colors with appropriate normalization technique
                if normalize(layer_name):
                    ch_min, ch_max = np.min(channel), np.max(channel)
                    norm_fn = self._color_norm_fn(ch_min, ch_max)
                    if norm_fn is not None:
                        try:
                            channel = norm_fn(channel)
                        except:
                            err_str = "Error normalizing colors for channel {} in {}"
                            print(err_str.format(layer_name, channel_idx))
                            print('Channel min: {}; max: {}'.format(ch_min, ch_max))
                
                start_row, stop_row = row*dim, (row+1)*dim
                start_col, stop_col = col*dim, (col+1)*dim
                layer_grid[start_row:stop_row, start_col:stop_col] = channel                                                    
                if seed_data is not None:
                    start_row, stop_row = row*seed_dim, (row+1)*seed_dim
                    start_col, stop_col = col*seed_dim, (col+1)*seed_dim
                    layer_seed_grid[start_row:stop_row, start_col:stop_col] = seed_data
                    
        return layer_grid, layer_seed_grid
            
            
    def _display_grid(self, 
                      layer_name, 
                      layer_grid, 
                      layer_seed_grid=None,
                      cmap=None, 
                      colorbar=False, 
                      title=True, 
                      fixed_scale=False,
                      savefig=False, 
                      fname_prefix=''):
       
        block_id = self._layer_block[layer_name]
        in_block_id = self._block_layers[block_id].index(layer_name)+1
        n_block_layers = len(self._block_layers[block_id])
        act_shape = ('N',) + self._activations[layer_name].shape[1:]

        scale = 1./32 if fixed_scale else 1./act_shape[1]
        
        
        spacing = 3.4 if colorbar else 3
        figsize = (scale * (layer_grid.shape[1]+spacing), 
                   scale * (layer_grid.shape[0]+3)) 
        
 
        #norm_used = self._color_norm_fn(np.min(grid), np.max(grid))
        #norm_used = None if norm_used is None else norm_used.__name__ 

        fig, ax = plt.subplots(sharex=True, sharey=True, figsize=figsize)
        plt.grid(False) 
        plt.xticks([])
        plt.yticks([])      
        
        if layer_seed_grid is None:
            ax.imshow(layer_grid, aspect='auto', cmap=cmap)
        else:
            ax.imshow(layer_seed_grid, aspect='auto', cmap='Greys')            
            ax.imshow(layer_grid, aspect='auto', cmap=cmap, alpha=0.6)           
        
        if colorbar:
            color_scale = colors.Normalize(vmin=0., vmax=1.)
            cbar_mappable = ScalarMappable(norm=color_scale, cmap=cmap)
            cbar_mappable.set_array(layer_grid)
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=.2, pad=0.1)
            cb = fig.colorbar(cbar_mappable, cax=cax, orientation="vertical") 
            
        if title:
            title = '{} (Layer {}/{} in block {} with shape {})'
            title = title.format(layer_name, 
                                 in_block_id, 
                                 n_block_layers, 
                                 block_id, 
                                 act_shape)
            ax.set_title(title, fontsize=10)  
            
        if savefig:
            fname = '{}-{}-{}.png'.format(layer_name, block_id, in_block_id)
            fig.savefig(fname_prefix + fname, dpi=400, facecolor='w', edgecolor='w', 
                        format='png', transparent=False, bbox_inches='tight', pad_inches=None)            
        plt.show()
        
    
    def _color_norm_fn(self, channel_min, channel_max):
       
        is_zeroish = lambda v: v == 0 or (v > 0 and v < self._zeroish)
        
        if is_zeroish(channel_min) and is_zeroish(channel_max):
            return None            
        else:     
            if self._lognorm_colors and channel_min >= 0:
                # Log scale
                norm_fn = colors.LogNorm(vmin=zeroish, 
                                         vmax=channel_max)
            elif self._lognorm_colors:
                # Symmetric log scale
                norm_fn = colors.SymLogNorm(linthresh=0.03, 
                                            linscale=0.03, 
                                            vmin=m, 
                                            vmax=M)
            else:
                # [0,255] scale
                norm = lambda ch: (ch-ch.mean()) / ch.std()
                adjust_norm = lambda ch: np.clip(64*norm(ch)+128, 0, 255)
                norm_fn = lambda ch: adjust_norm(ch).astype('uint8')
                norm_fn.__name__ = 'SimpleNorm'
                
        return norm_fn


    def _get_activation_model(self, model):
        """ """        
        self._layer_block = OrderedDict()
        self._model_layer_names = []
        
        curr_block = 0
        layer_outputs = []
        
        self._print('Extracting model layers...')        
        for l in model.layers:
            
            # Get outputs for activation model
            try:    
                layer_outputs.append(l.output)        
            except AttributeError:
                nodes = ksutils.find_nodes_in_model(model,  l)
                layer_outputs.append(l.get_output_at(nodes[0]))
              
            # Store layer and block information 
            if 'up_sampling' in l.name:
                curr_block += 1

            self._model_layer_names.append(l.name)
            self._layer_block[l.name] = curr_block         
            
            if 'pooling' in l.name:
                curr_block += 1
                
        # Create activation model with outputs for each layer
        self._print('Creating activation model with outputs for each layer...')        
        return models.Model(inputs=model.input, outputs=layer_outputs) 

       
    def _init_dict_attrs(self):
        
        self._block_layers = defaultdict(list)
        for layer_name, block_id in self._layer_block.items():
            self._block_layers[block_id].append(layer_name)
        
        self._nblocks = len(self._block_layers)
        self._last_block = self._nblocks - 1         

        ds_blocks = np.arange(0, self._nblocks//2)
        self._all_block_tuples = [(i, self._last_block-i) for i in ds_blocks]
                             
    
    def _set_fn_kwargs(self, fn, kwargs):
        arglist = inspect.getfullargspec(fn)[0]
        arglist.remove('self')
        fn_kwargs = {k:v for k,v in kwargs.items() if k in arglist}
        unspecified = set(arglist) - set(fn_kwargs.keys())        
        for arg in unspecified:
            val = getattr(self, '_{}'.format(arg), None)
            if val is not None:
                fn_kwargs[arg] = val
                
        return fn_kwargs
 
  
    def _print(self, *c):
        if self._verbose:
            print(*c)
                                  
                
